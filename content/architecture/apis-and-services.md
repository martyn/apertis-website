+++
title = "Platform APIs and Services"
weight = 100
toc = true
aliases = [
    "/old-developer/latest/apis.html",
    "/old-developer/latest/apps-core.html",
    "/old-developer/latest/connectivity.html",
    "/old-developer/latest/framework-app.html",
    "/old-developer/latest/framework-ui.html",
    "/old-developer/latest/infrastructure-ui.html",
    "/old-developer/latest/internet.html",
    "/old-developer/latest/multimedia.html",
    "/old-developer/latest/navigation.html",
    "/old-developer/latest/sdk-techs.html",
    "/old-developer/latest/telephony.html",
    "/old-developer/latest/telephony-ip.html",
    "/old-developer/v2019/apis.html",
    "/old-developer/v2019/apps-core.html",
    "/old-developer/v2019/connectivity.html",
    "/old-developer/v2019/framework-app.html",
    "/old-developer/v2019/framework-ui.html",
    "/old-developer/v2019/infrastructure-ui.html",
    "/old-developer/v2019/internet.html",
    "/old-developer/v2019/multimedia.html",
    "/old-developer/v2019/navigation.html",
    "/old-developer/v2019/sdk-techs.html",
    "/old-developer/v2019/telephony.html",
    "/old-developer/v2019/telephony-ip.html",
    "/old-developer/v2020/apis.html",
    "/old-developer/v2020/apps-core.html",
    "/old-developer/v2020/connectivity.html",
    "/old-developer/v2020/framework-app.html",
    "/old-developer/v2020/framework-ui.html",
    "/old-developer/v2020/infrastructure-ui.html",
    "/old-developer/v2020/internet.html",
    "/old-developer/v2020/multimedia.html",
    "/old-developer/v2020/navigation.html",
    "/old-developer/v2020/sdk-techs.html",
    "/old-developer/v2020/telephony.html",
    "/old-developer/v2020/telephony-ip.html",
    "/old-developer/v2021pre/apis.html",
    "/old-developer/v2021pre/apps-core.html",
    "/old-developer/v2021pre/connectivity.html",
    "/old-developer/v2021pre/framework-app.html",
    "/old-developer/v2021pre/framework-ui.html",
    "/old-developer/v2021pre/infrastructure-ui.html",
    "/old-developer/v2021pre/internet.html",
    "/old-developer/v2021pre/multimedia.html",
    "/old-developer/v2021pre/navigation.html",
    "/old-developer/v2021pre/sdk-techs.html",
    "/old-developer/v2021pre/telephony.html",
    "/old-developer/v2021pre/telephony-ip.html",
    "/old-developer/v2022dev0/apis.html",
    "/old-developer/v2022dev0/apps-core.html",
    "/old-developer/v2022dev0/connectivity.html",
    "/old-developer/v2022dev0/framework-app.html",
    "/old-developer/v2022dev0/framework-ui.html",
    "/old-developer/v2022dev0/infrastructure-ui.html",
    "/old-developer/v2022dev0/internet.html",
    "/old-developer/v2022dev0/multimedia.html",
    "/old-developer/v2022dev0/navigation.html",
    "/old-developer/v2022dev0/sdk-techs.html",
    "/old-developer/v2022dev0/telephony.html",
    "/old-developer/v2022dev0/telephony-ip.html",
    "/old-wiki/Packages",
]
date = "2016-04-20"
+++

Apertis follows an app-centric architecture. This means that the
development is more vertical and not layered as in automotive domain.
This reduces dependency and leads to less time to market.

SDK is designed to make app development fast and easy while maintaining
backward compatibility. The system gets upgraded periodically; however
the applications should not be affected. This is achieved by writing
apps against SDK APIs. The SDK APIs are ensured to maintained backward
compatibility.

The SDK will not only expose native system services integrated into the
OS, but also complete high level features which are of interest for
multiple Applications. The APIs are classified into Enabling APIs and SDK APIs.

- **SDK APIs**: Realize the high level features of the SDK behind the scene
  (e.g. an adressbook service which can be used by Apps to store and retrieve
  contact data in a joint place). They are dedicated APIs, provided with good
  documentation and sample code, easy to use and with backward compatibility
  guaranteed exposed with the intention to be used by the Apps.

- **Enabling APIs**: These are APIs from libraries/services that are typically
  taken from Open Source (for example, Telepathy, Tracker). Apps which use
  these APIs directly need to maintain the stability on their own due to API or
  ABI breakages. Effort may be made to ease or remove transition issues when
  ABI or API breakages happen.

  Trying to encapsulate these into SDK APIs will be a continuous process within
  Apertis.

- **OS APIs**: These are more fundamental APIs, usually used by the Enabling
  APIs to provide higher-level functionality. Platform upgrades may require
  adapting applications to new APIs and/or ABIs for these.

{{% notice info %}}
Please see the
[contribution process](contribution_process/#adding-components-to-apertis) for
guidance if your application depends on a component that is not including in
Apertis.
{{% /notice %}}

{{% notice note %}}
Many of the Apertis specific packages are named after sites in the UK where
significant Roman archaeological hoards were found.
{{% /notice %}}

# Core Applications

These are a basic set of applications that are bundled together with the system
and get updated with the system:

- **frampton**: Audio player
- **eye**: video player

# Infrastructure

Some infrastructure applications and services also need to be available as part of the
system to provide the basic UI even if there are no user focused applications:

## SDK services

- **mildenhall-launcher**: Every app installed in the system will be listed by
  this graphical application and be available to be launched.

  | Purpose | Git repository |
  |---------|----------------|
  | Application Launcher | <https://gitlab.apertis.org/pkg/mildenhall-launcher> |

- **mildenhall-popup-layer**: Graphical application for displaying popup
  notifications.

  | Purpose | Git repository |
  |---------|----------------|
  | Popup Notifications | <https://gitlab.apertis.org/pkg/mildenhall-popup-layer> |

- **mildenhall-settings**: Provides access to system and app settings.

  | Purpose | Git repository |
  |---------|----------------|
  | System settings | <https://gitlab.apertis.org/pkg/mildenhall-settings> |

- **mildenhall-statusbar**: Status information shown at the top of the screen

  | Purpose | Git repository |
  |---------|----------------|
  | Status Bar | <https://gitlab.apertis.org/pkg/mildenhall-statusbar> |


## Enabling Components
- [PolicyKit](https://www.freedesktop.org/software/polkit/docs/latest/polkit.8.html):
  System policy management
- [libsecret](https://developer.gnome.org/libsecret/0.16/): Secrets management

## OS APIs

- [eglibc](http://www.gnu.org/software/libc/manual/html_node/index.html): Basic OS interfaces
- [GLib](https://developer.gnome.org/glib/stable/): Basic programming framework
- [OSTree](http://ostree.readthedocs.io/): Atomic software updates
- [systemd](https://www.freedesktop.org/wiki/Software/systemd/): Service
  management service, used as both a system service and a user service. The
  system instance is process 1 (the first process launched by the kernel after
  leaving the early-boot environment), and is responsible for starting and
  monitoring all system services and all user sessions. The per-user instance,
  `systemd --user`, manages user services.

## Full package index

[source package indices](https://infrastructure.pages.apertis.org/dashboard/tsv/index.html):
Full package list in TSV format, categorized under different repository components, per platform release

# UI Framework

## Provides

- In apertis, Clutter library is provided for UI creation.
- Clutter is an open source software library for creating fast,
  compelling, portable, and dynamic graphical user interfaces.
- Use Open GLES for rendering.
- Comes with rich animation f/w with vast gesture support.
- Provides layout management for easy lay-outing of widgets.

## SDK services

- **libthornbury**: libthornbury is a bunch of user Interface utility
  libraries. Thornbury provides the helper function for customized JSON parsing
  which is used in view manager, texture creation for static .png files or
  download from the web sychronously/asynchronously using libsoup to create the
  texture using GdkPixbuf,View manager is intended to create/manage the views
  in an app using Clutter, ItemFactory is used to instantiate any given widget
  type which implements MxItemfactory. Model to give the MVC approach to the
  app inspired by ClutterModel.

  | Purpose | Git repository |
  |---------|----------------|
  | UI utility library | <https://gitlab.apertis.org/hmi/libthornbury> |

- **liblightwood**: libLightwood is a widget library.  It has all the basic
  widgets like buttons, lists, rollers, webviews, widgets, multilines and
  textboxes, and can be extended to add functionality to these base widgets.

  | Purpose | Git repository |
  |---------|----------------|
  | Widget library | <https://gitlab.apertis.org/hmi/liblightwood> | 

- **Mildenhall**: Provides the reference UI widgets, used by graphical applications.

  | Purpose | Git repository |
  |---------|----------------|
  | User interface widget library | <https://gitlab.apertis.org/hmi/mildenhall> |

## Enabling Components

- [Cairo](http://cairographics.org/): Drawing library
- [GTK+ 3](https://developer.gnome.org/gtk3/stable/): UI toolkit
- [Qt](https://www.qt.io/): UI toolkit

## OS APIs

- [gdk-pixbuf](https://developer.gnome.org/gdk-pixbuf/2.36/): Image manipulation
- [pixman](http://www.pixman.org/): Low-level graphics
- [Pango](https://pango.gnome.org/): High-level font rendering
- [Mesa](https://docs.mesa3d.org/): Low-level graphics
- [harfbuzz](https://www.freedesktop.org/wiki/Software/HarfBuzz/): Low-level font rendering
- [freetype](https://www.freetype.org/freetype2/docs/documentation.html): Low-level font rendering
- [JSON GLib](https://developer.gnome.org/json-glib/1.2/): (De)serialization of JSON
- [LLVM](http://llvm.org/docs/): Compiler technology
- [libxml2](http://www.xmlsoft.org/docs.html): XML Parsing
- [libxslt](http://xmlsoft.org/libxslt/docs.html): XML Transformation

# Application Framework

## Provides

- Launching a new Application (Launch-mgr)
- Explicit or implicit information (Combination of Action, URI, and
  MIME) can be used to determine an app to launch
- Application life cycle management (activity-mgr)
- Managing application launch history (LUM)
- Audio management and audio priority handling(audio-manager)
- Sharing mechanism
- Application preferences (prefernce-mgr)
- IPC communication (D-Bus)
- View switching and in app back handling (View-Manager)

## SDK services

- **Canterbury** : The core process in the system which deals with launch and
  kill of applications to be run on the system, including management of
  applications life cycle, creation and maintenance of application stack, etc.
  It also interacts on a regular basis with few other services for resource
  management of applications’, interaction with audio service (for audio
  handling and management), data exchange service (to facilitate
  inter-application data exchange) being 2 examples.

  | Purpose | Git repository |
  |---------|----------------|
  | Application management and process control service | <https://gitlab.apertis.org/appfw/canterbury> |

- **Didcot**: Service which mediates file opening by media type (content type,
  MIME type), URL opening by URL scheme, and other data sharing between user
  applications.

  | Purpose | Git repository |
  |---------|----------------|
  | Data sharing and file opening service | <https://gitlab.apertis.org/appfw/didcot> |

- **Newport**: It manages large downloads for applications and other services
  running on the system (browser, email, other devices, etc).

  | Purpose | Git repository |
  |---------|----------------|
  | Download manager | <https://gitlab.apertis.org/appfw/newport> |

- **Barkway**: This service caters to the global UI, providing a popup
  management framework, consisting of a user service and libraries that
  interface with it.

  | Purpose | Git repository |
  |---------|----------------|
  | Global popup management framework | <https://gitlab.apertis.org/appfw/barkway> |

- **libseaton**: Provides interfaces to store persistent data. It is a shared
  library using SQLite as the backend database.

  | Purpose | Git repository |
  |---------|----------------|
  | Persistent data management library | <https://gitlab.apertis.org/hmi/libseaton> |

- **libclapton**: Library which is used for system information and logging.

  | Purpose | Git repository |
  |---------|----------------|
  | Logging library | <https://gitlab.apertis.org/pkg/libclapton> |


- **Ribchester**: The service responsible for managing the application and
  services mount point and preparing the partitions based on the framework.

  | Purpose | Git repository |
  |---------|----------------|
  | Application installer/mounting service | <https://gitlab.apertis.org/pkg/ribchester> |

- **Rhosydd/Croesor**: Rhosydd and Croesor are system services for handling
  access to sensors and actuators from applications. It provides a
  vendor-specific hardware API to connect to the services which control the
  hardware, and a SDK API for applications to use to access it.

  | Purpose | Git repository |
  |---------|----------------|
  | Handling access to sensors and actuators | <https://gitlab.apertis.org/appfw/rhosydd> |
  | Sensor backend utilities                 | <https://gitlab.apertis.org/appfw/rhosydd> |

- **Shapwick**: Shapwick is a platform component which monitors resource use.
  It provides a system service and a library for communication with the
  service.

  | Purpose | Git repository |
  |---------|----------------|
  | Resource Monitoring | <https://gitlab.apertis.org/pkg/shapwick> |



## OS APIs

- [SQLite](https://sqlite.org/docs.html): Data storage
- [GMime](https://developer.gnome.org/gmime/stable/): File format support
- [D-Bus](https://www.freedesktop.org/wiki/Software/dbus/): `dbus-daemon` is a
  platform component, used as both a user service and a system service. The
  system bus, `dbus-daemon --system`, connects applications and platform
  components to system services, and mediates their access to those system
  services. All components that are able to connect to the system bus may
  listen for signals (normally broadcasts) from authorized system services. All
  other accesses, including sending signals, are prevented unless they are
  specifically allowed by configuration. The per-user session bus, `dbus-daemon
  --session`, is a user service which connects user applications and user
  services together and mediates their communication.

# Connectivity

## Provides

- Cellular and Wi-Fi Connection
  - Connman manages internet connections (Automatically connects to
    last used network).
- Bluetooth
  - Based on Bluez and profiles (PBAP, A2DP, PAN)
  - Discovering / bonding / getting contacts from cell phone

## SDK services

- Beckfoot: Beckfoot is performs connection management and handles
  system connectivity services like network handling, wifi management,
  management of Bluetooth devices etc.

## Enabling components

## OS APIs

- [Bluez](http://www.bluez.org/): Bluetooth technology

# Internet Services

## Provides

- http clients
- Connected services (PIM)

## Enabling components

- [libcurl](http://libcurl.org/): the multiprotocol file transfer library
- [libecal (Evolution Data Sever)](https://developer.gnome.org/libecal/stable/)

## OS APIs

- [Folks](https://wiki.gnome.org/Projects/Folks): Aggregation of contacts &
  calendaring information
- [Soup](https://developer.gnome.org/libsoup/stable/): HTTP client/server
  library for GNOME

# Multimedia

## Provides

- Playback of audio and video contents (local and streaming)
- Capturing images and recording audio and video
- Scanning & Playback of radio
- Complete audio management
- Extracting and displaying media content information
- Integration of External apps like VLC.

## SDK services

- **libgrassmoor** : libgrassmoor is a library responsible for providing
  media info and media playback functionalities

  | Purpose | Git repository |
  |---------|----------------|
  | Media information and playback library | <https://gitlab.apertis.org/hmi/libgrassmoor> |

- **Tinwell**: Is our Audio service. It focuses on complete audio handling
  for the system and provides facilities to play, buffer, record audio
  streams. It interacts with Canterbury to manage system audio
  effectively when multiple audio sources are active.

  | Purpose | Git repository |
  |---------|----------------|
  | Media playback service | <https://gitlab.apertis.org/hmi/tinwell> |

- **Prestwood**: This service manages media and handles any activity related to
  the removable media (such as mounting them), UPnP devices, etc and indicates
  when they are ready for other services to communicate with them.

  | Purpose | Git repository |
  |---------|----------------|
  | Disk mounting service | <https://gitlab.apertis.org/hmi/prestwood> |

## Enabling components

- Audio Manager: Audio Policy Management
- PulseAudio: Software mixing multiple audio streams
- Multiple-Format Codec: Various support of codec
- Media Content Service: Content management for media files
- [WebKitGTK+](https://webkitgtk.org/):  Web engine
- [Poppler](https://poppler.freedesktop.org/): PDF rendering
- [Canberra](http://0pointer.de/lennart/projects/libcanberra/gtkdoc/):
  High-level sounds
- [Grilo](http://developer.gnome.org/grilo/0.2/): Media indexing
- [Tracker-extract](https://developer.gnome.org/libtracker-extract/0.16/),
  [Tracker-miner](https://developer.gnome.org/libtracker-miner/0.16/),
  [Tracker-sparql](https://developer.gnome.org/libtracker-sparql/0.16/): Media
  indexing

# OS APIs

- [GStreamer](https://gstreamer.freedesktop.org/documentation/): Low-level multimedia

  {{% notice note %}}
  Apertis deploys only license verified Gstreamer plugins. Each Gstreamer plugin
  is developed under the [LGPL](https://www.gnu.org/licenses/lgpl.html) license,
  but only gstreamer [core](http://cgit.freedesktop.org/gstreamer/gstreamer),
  [base](http://cgit.freedesktop.org/gstreamer/gst-plugins-base) and
  [good](http://cgit.freedesktop.org/gstreamer/gst-plugins-good) provide license
  and patent-free plugins. The
  [ugly](http://cgit.freedesktop.org/gstreamer/gst-plugins-ugly) and
  [bad](http://cgit.freedesktop.org/gstreamer/gst-plugins-bad) plugins are not
  considered safe from the license and patent issues. The "bad"
  plugins usually have API stablity issues as well as license problems. In
  addition, the plugins which have potential issues with patents, or license fees
  should be located in "ugly". For more information, please refer to
  [gstreamers documentation](https://gstreamer.freedesktop.org/documentation/additional/splitup.html).
  {{% /notice %}}

# Navigation

## Provides

- Location based services
- Map
- Route guidance

## SDK services

- **Traprain**: Traprain is a set of libraries allowing navigation services,
  such as a car GPS routing application, to share navigation and routing
  information to third party applications.

  | Purpose | Git repository |
  |---------|----------------|
  | Navigation and routing libraries | <https://gitlab.apertis.org/appfw/traprain> |

## Enabling components

- Geoclue – Deliver location info from various positioning sources
  (http://freedesktop.org/wiki/Software/GeoClue/)
- libchamplain – Map rendering
  (https://wiki.gnome.org/Projects/libchamplain)

# Telephony

## Provides

- Call handling

## Enabling components

- [ofono](https://01.org/ofono) - includes consistent, minimal, and easy to use
  complete APIs for telephony

# VoIP, IM and Groupware

## Provides

- IP telephony
- Chat, *etc*.

## Enabling components

- Telepathy-GLib
  (http://telepathy.freedesktop.org/doc/telepathy-glib/)
- [Telepathy](http://telepathy.freedesktop.org/doc/book/index.html): Communication framework
- libGData (https://developer.gnome.org/gdata/unstable/)
- SyncEvolution D-Bus (http://api.syncevolution.org/)

## OS APIs
- [Farstream](https://www.freedesktop.org/wiki/Software/Farstream/#documentation): Communication

