+++
date = "2020-09-02"
weight = 100
toc = true

title = "Images"

aliases = [
	"/old-wiki/Images",
	"/old-designs/latest/release-v2019-artifacts.html",
        "/old-designs/v2019/release-v2019-artifacts.html",
        "/old-designs/v2020/release-v2019-artifacts.html",
        "/old-designs/v2021dev3/release-v2019-artifacts.html",
]
+++

Apertis has a
[release lifecycle]({{< ref "release-flow.md#apertis-release-flow" >}}) which
is roughly three months long.  Each release consists of a round of development,
testing and bug fixing, followed by a stable release.

{{% notice info %}}
The currently recommended versions for evaluation can be found on the
[download page]({{< ref "download.md" >}}).
{{% /notice %}}

Apertis provides images from daily build artifacts through to historic releases
(which have reached their end of life) for
[supported platforms]({{< ref "reference_hardware/_index.md" >}}). This allows
users and developers to both look at the latest changes as well as look at the
historic state, which can be useful when testing just merged fixes and tracking
down the origin of a bug respectively. The various available images can be
found on the [Apertis image site](https://images.apertis.org/).

{{% notice note %}}
Application and product developers should review the
[guidance for product development on top of Apertis]({{< ref "release-flow.md#guidelines-for-product-development-on-top-of-apertis-and-its-direct-downstreams" >}})
to help them come to an informed decision regarding the release that is
suitable for them.

It is recommended to take the
[release schedule]({{< ref "release-flow.md#apertis-release-flow" >}}) into
account when considering which release to develop against.
{{% /notice %}}

The main kinds of artifacts are:

* **ospack**: Rootfs without the basic hardware-specific components like bootloader, kernel and hardware-specific support libraries
* **system image**: combines an ospack with hardware-specific components in a snapshot that can be directly deployed on the supported boards
  * **OSTree-based images**: images with a immutable rootfs and a reliable update mechanism based on [OSTree](https://ostree.readthedocs.io/), more similar to what products would use than the Apt-based images
  * **Apt-based images**: images meant for development, with a modifiable rootfs that can be customized with the [Apt](https://wiki.debian.org/Apt) package manager
* **OSTree repository**: server backend used by the OSTree-based images for efficient distribution of updates

# Image Types

There are a number of types of images provided by Apertis, depending on the
platform, including:

## Minimal

Minimal images provide compact example images for headless systems and
are a good starting point for product-specific customizations.

Other than basic platform support in order to succesfully boot on the reference
hardware, the minimal example images ship the complete connectivity stack.

The reference update system is based on OSTree, but APT-based images are also
provided for development purposes.

No artifact covered by the GPLv3 is part of the `minimal` ospacks and images.

## Target

Target images provide the most complete superset of the features offered by the
minimal images, shipping full graphics support with a sample HMI running on top
and applications with full multimedia capabilities.

The reference update system is based on OSTree, but APT-based images are also
provided for development purposes.

No artifact covered by the GPLv3 is part of the `target` ospacks and images.

## Base SDK

The base SDK images are APT-based and meant to be run as
[VirtualBox]( {{< ref "/virtualbox.md" >}} ) VMs to provide a standardized,
ready-to-use environment for developers targeting Apertis, both for platform
and application development.

Since the SDK images are meant for development, they also ship tools covered by
the GPLv3 license.

## SDK

The full SDK images provide the same features as the base SDK images with
additional tools for application development using the Canterbury application
framework and the Mildenhall HMI.

## Sysroot

Sysroots are archived filesystem trees specifically meant for cross-compilation
and remote debugging targeting a specific release image.

They are meant to be read-only and target a specific release image, shipping
all the development headers and debug symbols for the libraries in the
release image.

Sysroots can be used to cross-compile for Apertis from a third-party
environment using an appropriate cross-toolchain. They are most suited for
early development phases where developers focus on quick iterations and rely
on fast incremental builds of their components.

The sysroots contain software covered by the GPLv3 as they are meant for
development purposes only.

See [sysroots amd devroots]({{< ref "sysroots-and-devroots.md#sysroot" >}}) for
more information about sysroots.

## Devroot

Devroots are archived filesystem trees meant to offer a foreign architecture
build environment via containers and binary emulation via the QEMU user mode.

They ship a minimal set of packages and offer the ability to install all the
packages in the Apertis archive.

Due to the nature of foreign architecture emulation they impose a considerable
overhead on build times compared to sysroot, but they avoid all the intricacies
that cross-building involves and offer the ability to reliably build deb packages
targeting foreign architectures.

See [sysroots amd devroots]({{< ref "sysroots-and-devroots.md#devroot" >}}) for
more information about when to use devroots.  For more informations about using
devroots see the
[tooling documentation]( {{< ref "tooling.md#development-containers-using-devrootenter" >}} ).

## NFS

The release includes artifacts for network booting using the TFTP and NFS protocols:
* kernel images for the reference architectures to be loaded via TFTP
* initrd with kernel modules matching the image to be loaded via TFTP
* DTBs (compiled device trees) for the reference hardware platforms to be loaded via TFTP
* rootfs tarball to be loaded via NFS
                                             
# Supported Artifacts

Here's a basic overview of the architectures and update mechanisms supported by
each image type:

| Type       | amd64 | armhf | arm64 | APT | OSTree | 
| ---------- | ----- | ----- | ----- | --- | ------ |
| Minimal    | ✅    | ✅    | ✅    | ✅  | ✅     |
| Target     | ✅    | ✅ \* |       | ✅  | ✅     |
| Base SDK   | ✅    |       |       | ✅  |        |
| SDK        | ✅    |       |       | ✅  |        |
| Sysroot    | ✅    | ✅    | ✅    |     |        |
| Devroot    | ✅    | ✅    | ✅    | ✅  |        |
| NFS        | ✅    | ✅    | ✅    | ✅  |        |

\* Since v2020
