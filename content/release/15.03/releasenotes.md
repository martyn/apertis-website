+++
date = "2015-11-30"
weight = 100

title = "15.03 ReleaseNotes"

aliases = [
    "/old-wiki/15.03/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Secure Automotive Cloud 15.03 Release

**15.03** is the current development distribution of **Secure Automotive
Cloud (SAC)**, a Debian/Ubuntu derivative distribution geared towards
the creation of product-specific images for ARM (ARMv7 using the
hardfloat API) and Intel x86 (64/32-bit Intel) systems. Features planned
for SAC can be found in the [Roadmap]() page.

### What's available in the distribution?

The software stack in **15.03** is comprised of the following
technologies:

  - Linux 3.16
  - Graphical subsystem based on Xorg X server 1.16.0 and Clutter 1.18.4
    with full Multi-Touch support
  - Network management subsytem provided by ConnMan 1.27, BlueZ 4.101
    and Ofono 1.14
  - Multimedia support provided by GStreamer 1.0.
  - The Telepathy framework, with XMPP and SIP support
  - The Folks contact management framework
  - The Clutter port of the WebKit browser engine with support for WebGL

### What's in the distribution?

  - Source Code Readiness
  - Evaluation of Wayland
  - Evaluation of Webkit2-GTK+
  - Multi-user concept design

### Release downloads

| [SAC 15.03 images]() |
| ----------------------------------------------------------------------------- |
| Intel 32-bit                                                                  |
| Intel 64-bit / Minnowboard MAX                                                |

  - SAC 15.03 repositories:

` deb `https://repositories.secure-automotive-cloud.org/sac/` 15.03 target development sdk hmi`

  - SAC 15.03 infrastructure tools:

For Debian Jessie based systems:

` deb `https://repositories.secure-automotive-cloud.org/debian/` jessie tools`

For Ubuntu Trusty based systems:

` deb `https://repositories.secure-automotive-cloud.org/ubuntu/` trusty tools`

# SAC 15.03 Release

This release builds on the Ubuntu 14.10 (Utopic Unicorn) as base
distribution. The 15.03 release has been verified in the [15.03 test
report]().

## Source Code Readiness

Several applications and services have been moved to public git
repositories at `https://git.secure-automotive-cloud.org/` The released
projects include:

  - Barkway - System popup management and display service
  - Beckfoot - Network interface management service
  - Canterbury - Application management and process control service
  - Chalgrove - Application and service management service
  - Corbridge - Bluetooth management service
  - Didcot - Data sharing and file opening service
  - Eye - Video player application
  - Frampton - Audio player application
  - Libclapton - System information and logging library
  - Libgrassmoor - Media information and playback library
  - Liblightwood - Widget library
  - Libseaton - Persistent data management library
  - Libthornbury - UI utility library
  - Mildenhall-launcher - Application launcher application
  - Mildenhall-mutter-plugin - Window management plugin
  - Mildenhall-popup-layer - Popup display application
  - Mildenhall-settings - System and application configuration
    application
  - Mildenhall-statusbar - Status bar application
  - Mildenhall - UI widget library
  - Newport - Download manager application
  - Plymouth-theme-mrs - MRS plymouth theme
  - Prestwood - Disk mounter application
  - Rhayader - Web browser application
  - Ribchester - Application mounter
  - Shapwick - Resource monitor application
  - Tinwell - Media playback service

Build packages have been created in OBS as well, to be included in
package repositories and images.

## Webkit2GTK+ Evaluation

A document explaining the need for switching away from WebKit Clutter
and assessing the move to WebKit2GTK+ was written and discussed during
2015Q1. Its recommendations have been accepted and work is beginning to
make the move a reality now, with a roadmap and a very basic release to
be delivered in Q2.

## Wayland Evaluation

A document explaining the need for reasons for away from X11 and
assessing the move to a wayland ased compositor system was written and
discussed during 2015Q1
([WaylandEvaluation]( {{< ref "wayland_evaluation.md" >}} )). Its
recommendations have been accepted and work is beginning to make the
move a reality now, with a roadmap and a first alpha release to be
delivered later this year.

## Multi-user concept document

Updated a design document for some of the possible multi-user use cases,
with a particular focus on rapid switching between non-concurrent users
so that a passenger can assist the driver: [Multi-User design,
version 0.4.1]()

## Infrastructure

During Q1 cycle, several activities have been carried out to be able to
set all SAC infrastructure in place. Most interesting remarks follow
below:

### OBS Build Projects

At Collabora's OBS instance:

  - [OBS SAC 15.03
    Target](https://build.collabora.co.uk/project/show?project=sac%3A15.03%3Atarget)
  - [OBS SAC 15.03
    Development](https://build.collabora.co.uk/project/show?project=sac%3A15.03%3Adevelopment)
  - [OBS SAC 15.03
    SDK](https://build.collabora.co.uk/project/show?project=sac%3A15.03%3Asdk)
  - [OBS SAC 15.03
    HMI](https://build.collabora.co.uk/project/show?project=sac%3A15.03%3Ahmi)
  - [OBS SAC Infrastructure
    Tools](https://build.collabora.co.uk/project/show?project=sac%3Ainfrastructure)

### Shared Repositories

Repositories are found at:

` deb `https://repositories.secure-automotive-cloud.org/sac/` 15.03 target development sdk hmi`

To be able to access those, a temporary user/passwd has been created
until properly publicly published: Username: **sac-dev** Password:
**Apa8Uo1mIeNgie6u**

### Images

Image daily builds, as well as release builds can be found at:

` `https://images.secure-automotive-cloud.org/

*Use your Collabora credentials to access those until those get publicly
published.*

Image build tools can be found in the SAC tools repositories. Note that
a string is added to package version depending on the distribution suite
based on. For example, if trusty system is to be used expect to install
image-builder_4trusty1

|                                               |                  |
| --------------------------------------------- | ---------------- |
| **Package**                                   | **Version**      |
| image-builder, image-tools                    | 4                |
| sac-image-configs, sac-image-scripts          | 6                |
| linaro-image-tools, python-linaro-image-tools | 2012.09.1-1co38  |
| parted, libparted0debian1                     | 2.3-11ubuntu1co3 |
| python-debian                                 | 0.1.25           |

### Test Framework

LAVA service at Collabora triggers test cases upon image builds, service
is found at:

` `<https://lava.collabora.co.uk/>

The list of available test cases, including those can be found
[here]( {{< ref "/qa/test_cases" >}} ).

LAVA service packages are available in the SAC tools repository. To be
able to install it, please follow
[instructions]()

### Mailing List

A mailing list setup and configuration has been deferred for next
quarter release (15.06).

## Known issues

### AppArmor tests

  - The *apparmor-functional-demo* test is partially failing. This is a
    bug in the test, not in the functionality: fixes are believed to be
    ready for Q2 ()
  - The *apparmor-geoclue* test occasionally stalls without output, in a
    way that blocks other testing from proceeding. It has been omitted
    from regular automated testing and will be further investigated
    during Q2 (, )
  - The *apparmor-gstreamer1.0* test timed out on ARM, with no output
    within 90 seconds. It is not clear why. This will be further
    investigated during Q2 ()
  - The *apparmor-webkit-clutter* test intermittently fails with an X11
    error. This is a failure to start the webkit-clutter browser used
    for the test, not a failure in AppArmor's confinement of it. ()
  - Xorg and many of the components originating in appfw/guifw have
    incomplete AppArmor profiles, which log "complaints" to the audit
    log for operations that should be allowed without complaint. This
    might cause manual AppArmor tests to fail when an unrelated
    component triggers complaints at the wrong time; for instance, we
    think this is what is happening in . For now, developer
    interpretation of log files will be needed to determine whether
    these AppArmor complaints should be counted as a real test failure
    or not. (Bug reports for 15.06 requesting more complete AppArmor
    profiles during Q2:           )

### Other tests

  - The *folks-extended-info* test intermittently fails. This appears to
    be a race condition in the Folks contact library: the extended info
    is correctly saved to the address book, and the test expects that
    the new extended info is signalled to the application immediately,
    but in fact it is sometimes signalled to the application a few
    main-loop iterations later. In GUI applications, this will not have
    any noticeable effect in practice. ()
  - The *flickr* test-case in the *librest* test failed on ARM. It is
    not clear why. This will be further investigated during Q2 ()
  - Two test cases in the *webkit-clutter-javascriptcore* test set fail.
    Given: 1) this has not materialized in browser misbehaviour that
    blocks development 2) our priority shifted towards a move to
    WebKit2GTK+, and 3) a new upstream merge will land after 15.03,
    Collabora decided not to spend time on this bug for now ()
  - All test cases in the *webkit-clutter-layout* test failed on LAVA.
    This was also happening on a regular image running on a VM due to a
    bug in the test tool causing it to look for the DejaVu fonts at the
    wrong path. That bug was fixed and tests now work on a regular VM,
    but fail on LAVA. This will be further investigated during Q2 ()
  - The *telepathy-ring-sms* test has been seen failing with error "SMS
    service is not available" in some setups. This will be further
    investigated during Q2 ()
