+++
date = "2021-06-17"
weight = 100

title = "v2020.6 Release schedule"
+++

The v2020.6 release cycle started in June 2020.

| Milestone                                                                                                | Date              |
| -------------------------------------------------------------------------------------------------------- | ----------------- |
| Start of release cycle                                                                                   | 2021-06-17        |
| Soft feature freeze: end of feature proposal and review period                                           | 2021-08-18        |
| Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed | 2021-08-25        |
| Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date              | 2021-09-01        |
| RC testing                                                                                               | 2021-09-02..06-08 |
| v2020.6 release                                                                                          | 2021-09-09        |

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/policies/releases.md" >}} ) and
    more information about the timeline
