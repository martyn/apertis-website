+++
date = "2016-09-27"
weight = 100

title = "user_ofono-voicecall"

aliases = [
    "/user_connman-ofono-voicecall.md",
    "/old-wiki/User:Connman-ofono-voicecall",
    "/qa/test_cases/ofono-voicecall.md",
    "/old-wiki/QA/Test_cases/ofono-voicecall",
    "/old-wiki/User:Ofono-voicecall"
]
+++

This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
