+++
date = "2021-06-25"
weight = 100
toc = true

title = "Application distribution using Flatpak"
+++

# Apertis Flatpak demo

Apertis provides a [demo](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-demo)
Flatpak application. This demo includes a runtime containing the libraries required to run the
[GNOME Fonts](https://gitlab.gnome.org/GNOME/gnome-font-viewer) application.

In order to install the demo application, the Flatpak repositories for both the
runtime and application must first be setup:

```
$ flatpak --user remote-add --no-gpg-verify apertis-demo-runtime https://images.apertis.org/flatpak/runtime
$ flatpak --user remote-add --no-gpg-verify apertis-demo-app https://images.apertis.org/flatpak/app
```

You can then proceed with the installation:

```
$ flatpak --user install org.apertis.demo.gnome_font_viewer
```

{{% notice warning %}}
During installation you will be prompted for which version to install. Due to
incompatibilities between flatpak versions, it is highly recommended you select
the version corresponding to the Apertis version your system is running.
{{% /notice %}}

Finally, run the application by executing the following command:

```
$ flatpak run org.apertis.demo.gnome_font_viewer
```

# Creation

The preferred way of creating Flatpak runtimes and applications for Apertis is
by using `flatdeb`, which automates most of the creation process and can
generate ready-to-use runtimes and applications with a few command invocations.

## Prerequisities

`flatdeb` can be installed by cloning its [Git repository](https://gitlab.apertis.org/pkg/apertis-flatdeb.git)
and installing its dependencies as mentioned in the
[README](https://gitlab.apertis.org/pkg/apertis-flatdeb/-/blob/apertis/v2022dev3/README) file.

Once `flatdeb` has been downloaded, create a new folder (outside the `flatdeb`
source tree) in which you will create your runtime and application.

First you need to create a `suite` definition so `flatdeb` knows which package
repositories to use. Create a new `suites` folder, then a new file in this folder,
named after the Apertis version you're targetting: for Apertis `v2022dev2`, the
file will be `suites/v2022dev2.yaml`. You can simply copy
[this template file](https://gitlab.apertis.org/infrastructure/apertis-flatdeb-demo/-/blob/master/suites/template.yaml)
which already contains everything needed for an Apertis suite, and name it
appropriately.

Once the suite is configured, we need to generate a base chroot for flatdeb to
work in. From the working directory, please execute the following command:

```
$ /path/to/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir \
                                  --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo \
                                  --suite=<SUITE_NAME> --arch=<TARGET_ARCHITECTURE> \
                                  base
```

This will create a `flatdeb-builddir` subfolder containing a file named
`base-<SUITE_NAME>-<TARGET_ARCHITECTURE>.tar.gz` containing the base rootfs
needed for future operations. If you delete this file, or want to target a
different Apertis version, you will have to run the above command again.

## Runtime

By using `flatdeb`, one can create and populate a Flatpak runtime by using
available `.deb` packages. This makes the runtime creation simpler and faster by
removing the need to build everything from source and automating dependency
management.

The runtime is described in a yaml file located in a `runtimes` subfolder and
containing the following elements:
- `id_prefix`: the runtime unique identifier in [reverse domain name notation](https://en.wikipedia.org/wiki/Reverse_domain_name_notation)
- `add_packages`: the list of binary packages to be included in the runtime
- `add_packages_multiarch`: the list of multiarch-capable packages (typically,
  those are shared libraries) to be included in the runtime
- `sdk`: this section also contains `add_packages` and (optionally)
  `add_packages_multiarch` items; those are the packages needed for building
  the application, but are not required to run it.

The runtime recipe may also contain a `pre_apt_script` element, used for
configuring the flatdeb environment before installing the packages. In the demo
application, this script is used to add the `development` repository, as some of
the required packages are only present in `development` and not in `target`,
which happens to be the base system of the runtime.

The packages listed under `add_packages` in the `sdk` section are those needed
for building the application. That includes development libraries and headers,
the build system and compilers, and optionally additional development tools.

The packages listed under the main `add_packages*` sections are the runtime
dependencies of the application. Those are mainly shared libraries, but they can
also include executables the application would depend on, or even resources for
making the application more useful. In the demo application, we added a number
of fonts packages: as this application is a font viewer, we can therefore ensure
the application can effectively find and display all installed fonts.

Once the runtime recipe is complete, you can generate the runtime with the
following commands:

```
$ /path/to/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir \
                                  --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo \
                                  --suite=<SUITE_NAME> --arch=<TARGET_ARCHITECTURE> \
                                  --platform runtimes runtimes/<RUNTIME_NAME>.yaml
$ /path/to/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir \
                                  --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo \
                                  --suite=<SUITE_NAME> --arch=<TARGET_ARCHITECTURE> \
                                  --sdk runtimes runtimes/<RUNTIME_NAME>.yaml
```

This will generate first the `Platform` runtime, which is needed for running the
application, then the `Sdk` runtime, which is needed for building the
application. Those will both be stored in an OSTree repository under
`flatdeb-builddir/ostree-repo`.

## Application

Applications are generated from a more classic
[Flatpak manifest](https://docs.flatpak.org/en/latest/manifests.html), the only
notable points here being that:
- `runtime` must mention your runtime as `<id_prefix>.Platform`
- the same goes for `sdk` which should contain `<id_prefix>.Sdk`
- `runtime-version` will contain the suite name (Apertis release)

The manifest should be located under a new subfolder named `apps`.

The application is generated by executing the following command:

```
$ /path/to/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir \
                                  --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo \
                                  --suite=<SUITE_NAME> --arch=<TARGET_ARCHITECTURE> \
                                  app --app-branch=<SUITE_NAME> apps/<APP_NAME>.yaml
```

Once the command completes, the application will be available from the same OSTree
repository already containing the runtime, under `flatdeb-builddir/ostree-repo`.
Serving this directory through an HTTP server is enough to distribute your
Flatpak runtime and application.

# Verification

Starting with Apertis v2022dev2, [Flatpak](https://flatpak.org) includes the
ability to distribute [application bundles]( {{< ref "glossary.md#application-bundle" >}} )
verified with [ed25519](https://ed25519.cr.yp.to/) signatures.

{{% notice warning %}}
The current implementation is a technology preview and it is expected to stabilize
during the release cycles leading to the v2022 stable release. The prototype is
already available in Apertis as documented here, but it may be subject to potentially
incompatible changes during the [upstream review process](https://github.com/flatpak/flatpak/pull/4170).
{{% /notice %}}

This signature system relies on OSTree's library functions. Therefore, the key
generation and storage process is identical to what is described in the
[System updates and rollback]( {{< ref "system-updates-and-rollback.md#verified-updates" >}} )
design document.

Flatpak application signatures occur on several levels:
  * single commits
  * whole repositories
  * single-file bundles

Please note, however, that GPG signatures are disabled on Apertis. It is still
possible to pull from GPG-signed repositories, but those signatures won't be
verified. Similarly, it is not possible to sign flatpak applications using GPG.

## Creating signed flatpak applications

The simplest way to create a signed flatpak is to use `flatpak-builder` with
the `--sign=<SECRETKEY>` command-line argument, where `<SECRETKEY>` is the
base64-encoded secret Ed25519 key. This ensures the OSTree commit and summary
are properly signed:

```
flatpak-builder --repo=myrepo --sign=m8/rp9I9ax2w81yujZyeXTfZlbeBjEBUPQSQKo14iHgHdrzpKYH6xvL83midrFNeMrU4QBtk4jZ+x2veQoP4oQ== build-dir org.example.sampleapplication.yaml
```

For more advanced usage, the same command-line option can also be used with the
following flatpak commands:
  * `flatpak build-bundle`
  * `flatpak build-commit-from`
  * `flatpak build-export`
  * `flatpak build-import-bundle`
  * `flatpak build-sign`
  * `flatpak build-update-repo`

These commands allow one to create Ed25519-signed commits from an unsigned
repository or bundle, or to create signed bundles as explained below.

Multiple occurrences of the `--sign` option are allowed in to order to permit
multiple signatures of each object.

More details about those commands are available in the
[Flatpak documentation](https://docs.flatpak.org/en/latest/building.html).

## Publishing signed flatpaks applications

### Publishing a repository

When distributing several applications, it can be useful to publish the whole
repository using a [.flatpakrepo](https://docs.flatpak.org/en/latest/hosting-a-repository.html#flatpakrepo-files)
file.

The only difference here is that the `GPGKey=...` line must be replaced with
`SignatureKey=<PUBLICKEY>`, where `<PUBLICKEY>` is the base64-encoded public
Ed25519 key.

Such a .flatpakrepo file could be:

```
[Flatpak Repo]
Title=Sample Repository
Url=https://example.org/flatpak/repo
Homepage=https://example.org/flatpak
Comment=Sample Flatpak repository signed with Ed25519
Description=This Flatpak repository provides applications signed with Ed25519
Icon=https://example.org/flatpak/icon.svg
SignatureKey=B3a86SmB+sby/N5onaxTXjK1OEAbZOI2fsdr3kKD+KE=
```

### Publishing a single application

One convenient way to distribute single flatpak applications is to use
[.flatpakref](https://docs.flatpak.org/en/latest/repositories.html#flatpakref-files)
files. Those files include all necessary information for flatpak to be able to
install and update the application.

Exactly as it is done with with `.flatpakrepo` files, using
`SignatureKey=<PUBLICKEY>` instead of `GPGKey=...` will instruct flatpak to
enable Ed25519 signature verification for this repository.

This line will instruct flatpak to add the corresponding configuration keys to
the remote and perform signature verification when installing and/or updating
this application.

Such a .flatpakref file could be:

```
[Flatpak Ref]
Name=org.example.sampleapplication
Title=Sample application from our example repo
Url=https://example.org/flatpak/repo
RuntimeRepo=https://example.org/flatpak/example.flatpakrepo
IsRuntime=false
SignatureKey=B3a86SmB+sby/N5onaxTXjK1OEAbZOI2fsdr3kKD+KE=
```

### Publishing a bundle

Flatpak applications can also be distributed as
[single-file bundles](https://docs.flatpak.org/en/latest/single-file-bundles.html),
which can be created using the `flatpak build-bundle` command. As previously
mentioned, these bundles can be signed by adding the `--sign=<SECRETKEY>` option
to the command invocation:

```
flatpak build-bundle --sign=m8/rp9I9ax2w81yujZyeXTfZlbeBjEBUPQSQKo14iHgHdrzpKYH6xvL83midrFNeMrU4QBtk4jZ+x2veQoP4oQ== myrepo example.bundle org.example.sampleapplication
```

However, when publishing a signed flatpak bundle, the corresponding public key
has to be stored in a location easily accessible to the final user for signature
verification, as the bundle file itself is signed and doesn't provide any mean
to retrieve the associated public key.

## Installing a signed flatpak

### Configuring a remote repository

If the repository publisher provides a `.flatpakrepo` file including the public key,
then no action is needed other than running `flatpak remote-add <REPONAME> <REPOFILE>`.

However, if such a file is not available, one must add the `--sign-verify`
command-line option to the `flatpak remote-add` command in order to provide
either the public key directly, or a file containing the public key:
  * `--sign-verify=ed25519=inline:<PUBLICKEY>` is used to directly specify the
    public key needed to verify this repository
  * `--sign-verify=ed25519=file:<PATH>` can be used to point flatpak to a file
    containing a list of public keys (base64-encoded, one key per line), among
    which at least one can be used to verify signatures for this repository

```
flatpak remote-add example example.flatpakrepo
```

or

```
flatpak remote-add --sign-verify=ed25519=inline:B3a86SmB+sby/N5onaxTXjK1OEAbZOI2fsdr3kKD+KE= example https://example.org/flatpak/repo
```

Multiple `--sign-verify` occurrences are allowed in order to specify as many
public keys as needed. This can be useful when a new signature key is being
deployed, while the old one is still in use: by specifying both the old and the
new key, users can make sure at least one of those will be able to verify the
signatures. That way, once the old key is revoked and only the new one is used
for signing the repository, the corresponding remote will keep working as
expected.

This option can also be added when using the `flatpak remote-modify` command.

### Installing a signed application

Similarly to the process of using `.flatpakrepo` files, when installing a single
application using a `.flatpakref` file including the public key, no additional
action is needed. Flatpak will automatically verify Ed25519 signatures using the
provided public key:

```
flatpak install --from example.flatpakref
```

When the application is installed from a previously configured repository,
signature verification is also automated, as long as the corresponding public
key has been imported into the remote's configuration:

```
flatpak install org.example.sampleapplication
```

If the public key has not been previously imported into the remote's
configuration, one can also use the `--sign-verify` command-line option:

```
flatpak install --sign-verify=ed25519=inline:B3a86SmB+sby/N5onaxTXjK1OEAbZOI2fsdr3kKD+KE= org.example.sampleapplication
```

### Installing a signed bundle

Flatpak bundles are not installed from a repository like most flatpak
applications, but from a single, optionally signed, file. As there is no
repository configuration to import public keys from, the user needs to specify
the relevant public keys using the `--sign-verify` command-line option as stated
above.

```
flatpak install --sign-verify=ed25519=inline:B3a86SmB+sby/N5onaxTXjK1OEAbZOI2fsdr3kKD+KE= --bundle example.bundle
```

This option works the same way with both `flatpak build-import-bundle` and
 `flatpak install` commands.

# References

Flatpak reference documentation: <https://docs.flatpak.org/>
