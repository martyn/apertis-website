+++
date = "2020-12-15"
weight = 100

title = "Automated Licensing Compliance Install"
+++

# Automated Licensing Compliance Install

As described in [Automated License Compliance]( {{< ref "automated-license-compliance.md" >}}) a check is performed on image creation to confirm the compliance with the terms that [Apertis may wish to avoid]( {{< ref "license-expectations.md" >}} ).

To support this feature several pieces of software need to be installed.

## FOSSology

The recommended FOSSolgy setup consist in
- FOSSology server
- PostgreSQL database server
- Apache HTTP server

We recommend to build this setup trough a Docker installation as described in [FOSSolgy Docker installation](https://github.com/fossology/fossology/blob/master/README.md#docker)

Bellow there is a sample Docker compose file to spawn the different services

```
# FOSSology Docker Compose file
# Copyright Siemens AG 2016, fabio.huser@siemens.com
# Copyright TNG Technology Consulting GmbH 2016-2017, maximilian.huber@tngtech.com
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#
# Description: Recipe for setting up a multi container FOSSology
#              Docker setup with separate Database instance
version: '3.5'
services:
  scheduler:
    image: fossology/fossology:3.9.0
    restart: unless-stopped
    environment:
      - FOSSOLOGY_DB_HOST=db
      - FOSSOLOGY_DB_NAME=fossology
      - FOSSOLOGY_DB_USER=fossy
      - FOSSOLOGY_DB_PASSWORD=fossy
    command: scheduler
    depends_on:
      - db
    volumes:
      - repository:/srv/fossology/repository/
  web:
    image: fossology/fossology:3.9.0
    restart: unless-stopped
    environment:
      - FOSSOLOGY_DB_HOST=db
      - FOSSOLOGY_DB_NAME=fossology
      - FOSSOLOGY_DB_USER=fossy
      - FOSSOLOGY_DB_PASSWORD=fossy
      - FOSSOLOGY_SCHEDULER_HOST=scheduler
    command: web
    ports:
      - 8081:80
    depends_on:
      - db
      - scheduler
    volumes:
      - repository:/srv/fossology/repository/
  db:
    image: postgres:9.6
    restart: unless-stopped
    environment:
      - POSTGRES_DB=fossology
      - POSTGRES_USER=fossy
      - POSTGRES_PASSWORD=fossy
      - POSTGRES_INITDB_ARGS='-E SQL_ASCII'
    volumes:
      - database:/var/lib/postgresql/data

volumes:
  database:
  repository:
```

## Gitlab CI

The license scanning process is triggered by Gitlab CI, which requires the latest version of the following recipes:

- `ci-package-builder`
- `apertis-docker-images`
- `apertis-image-recipes`

The following CI variables configure the parameters to connect with FOSSology sever
- `FOSSOLOGY_URL`
- `FOSSOLOGY_USERNAME`
- `FOSSOLOGY_PASSWORD`

## OBS

The package build process is performed in OBS by running `debhelper`, which requires the version `12.1.1co1` or above.


