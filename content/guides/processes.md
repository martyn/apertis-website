+++
title = "Processes"
weight = 100

aliases = [
    "/old-developer/latest/programming-guide-processes.html",
    "/old-developer/v2019/programming-guide-processes.html",
    "/old-developer/v2020/programming-guide-processes.html",
    "/old-developer/v2021pre/programming-guide-processes.html",
    "/old-developer/v2022dev0/programming-guide-processes.html",
    "/old-wiki/Guidelines/Processes"
]
date = "2016-12-12"
+++

There are various things which affect an entire application process.  They are
not all related, so are covered in separate subsections; some apply to all
applications, others to specific functionality.

# Summary

- Use libraries instead of [subprocesses]( {{< ref "#subprocesses" >}} ) where
  possible.
- Use closed-loop subprocess management to keep track of when
  [subprocesses]( {{< ref "#subprocesses" >}} ) exit.
- Do **not** use `sigaction()`:
  [use `g_unix_signal_add()`]( {{< ref "#unix-signals" >}} ) instead.
- Do **not** use `sleep()`:
  [use `g_timeout_add_seconds()`]( {{< ref "#sleeping" >}} ) instead.
- Do **not** use `system()`:
  [use libraries or single subprocesses]( {{< ref "#shell-commands" >}} )
  instead.
- [Use `GApplication` and `GOptionContext`]( {{< ref "#command-line-parsing" >}} )
  to do command line parsing.

# Subprocesses

Launching subprocesses is sometimes necessary to perform specific tasks, though
where possible it is generally better to link against and use an appropriate
library to perform the same task, as it eliminates a lot of potential failure
points, simplifies passing data around (as passing data to subprocesses
involves pipes, and is complex), and generally allows for greater interaction
between the parent process and the task.

Subprocesses should not be used to implement functionality which could be
implemented by [D-Bus service daemons]( {{< ref "d-bus_services.md" >}} ).

When using subprocesses, closed-loop management should be used, by monitoring
the child process’ PID after sending it kill signals, and waiting until the
child process terminates before proceeding with further processing. The
alternative, open-loop management, sends signals and never checks to see if the
child responded to them, which is more fragile.

To do this, use
[`g_spawn_async()`](https://developer.gnome.org/glib/stable/glib-Spawning-Processes.html)
to spawn child processes, and
[`g_child_watch_add()`](https://developer.gnome.org/glib/stable/glib-The-Main-Event-Loop.html)
to set up a callback to be invoked when the child exits.

# UNIX signals

The standard UNIX function for setting up signal handlers,
[`sigaction()`](http://man7.org/linux/man-pages/man2/sigaction.2.html), **must
not** be used in GLib programs, as its callback must be re-entrancy safe, and no
GLib code is re-entrancy safe. Instead,
[`g_unix_signal_add()`](https://developer.gnome.org/glib/stable/glib-UNIX-specific-utilities-and-integration.html#g-unix-signal-add)
should be used, which doesn’t require its callback to be re-entrancy safe.

# Sleeping

The
[`sleep()`](http://pubs.opengroup.org/onlinepubs/009695399/functions/sleep.html)
function **must not** be used in GLib code, as it blocks the current thread.
Instead,
[`g_timeout_add_seconds()`](https://developer.gnome.org/glib/stable/glib-The-Main-Event-Loop.html#g-timeout-add-seconds)
or
[`g_timeout_add()`](https://developer.gnome.org/glib/stable/glib-The-Main-Event-Loop.html#g-timeout-add)
should be used to schedule a callback to be invoked after a period of time; in
the mean time, events can continue being processed by the main context.

# Shell commands

The [`system()`](http://linux.die.net/man/3/system) function **must not** be
used. It is extremely prone to
[shell injection vulnerabilities](http://en.wikipedia.org/wiki/Code_injection#Shell_injection),
and any functionality which can be implemented using it can be implemented
using libraries directly or, if that is not possible, spawning individual
[subprocesses]( {{< ref "#subprocesses" >}} ).

For example, instead of calling `system (g_strdup_printf ("mv %s %s",
file_path1, file_path2))`, use
[`g_file_move()`](https://developer.gnome.org/gio/stable/GFile.html#g-file-move).
In this situation, using `system()` exposes a shell injection vulnerability,
and will not work with paths containing spaces in any case.

# Command line parsing

Command line parsing should be implemented using
[`GApplication`](https://developer.gnome.org/gio/stable/GApplication.html) and
[`GOptionContext`](https://developer.gnome.org/glib/stable/glib-Commandline-option-parser.html),
rather than being implemented manually or using another library (such as
[popt](http://directory.fsf.org/wiki/Popt)).

By using `GOptionContext`, command line parsing follows the standard format
used by most Linux applications. It gives automatic support for `--help`
output, and short and long option parsing.

An example of using `GApplication` together with `GOptionContext` is
[here](https://gitlab.gnome.org/GNOME/glib/-/blob/main/gio/tests/gapplication-example-cmdline3.c).

# External links

- [Article on shell injection vulnerabilities](http://en.wikipedia.org/wiki/Code_injection#Shell_injection)
