+++
date = "2016-01-18"
weight = 100

title = "Conditional Access"

aliases = [
    "/old-wiki/Conditional_Access",
    "/old-wiki/Conditional_Access/Implementation",
]

status = "Requires Update"
statusDescription = "This concept is an old work in progress. It assumes Apertis device is in a vehicle. Acronyms such as DLC used with no description nor link."
+++

App-store curators and ISVs often impose technical restrictions on
applications (and other content, such as themes) so that they can only
be used by a user who has purchased the application, or on a device for
which a license has been purchased. This design is intended to address
this topic.

# Assumptions

## Identification

### Store account ID

For the purposes of this design, we assume that the owner(s) of the
vehicle has/have access to at least one store account. The platform may
support Apertis-based operating systems (OEM variants) without app-store
support, but none of the scenarios given here are required to work on
those variants, with the possible exception of [Unrestricted
app-bundle](#unrestricted-app-bundle).

We assume that each [store account]( {{< ref "/glossary.md#store-account" >}} )
has an unique identifier, which never changes. This unique identifier is
treated as opaque here: we do not assume that it corresponds to the
credentials used to log in. For example, the user might log in with an
email address or username and a user-chosen password, but their unique
identifier might be an integer or a
[UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier),
allowing the user to associate a different email address or username
with the same store account identifier at a later date.

For example, suppose Adam authenticates to the app-store using his email
address adam@example.com and a password. The opaque unique identifier
corresponding to adam@example.com might be S123.

We assume that each [user account (as used for multi-user
support)]( {{< ref "/glossary.md#user-account" >}} ) is associated with up to
one store account at any given time. For example, this matches the model
of accounts used on the PlayStation 3 and 4, with Sony Entertainment
Network accounts corresponding to our store accounts.

For example, suppose Adam controls store account S123, and also store
account S456 (perhaps he accidentally created separate store accounts
for two different cars). On his Apertis system, suppose his user account
is U1000, currently linked to store account S123. We assume that it is
possible for Adam to change U1000 so it is linked to S456 instead, but
we assume that associating it with both S123 and S456 at the same time
is impossible.

<strong id="multiple-store-accounts">Possible future addition: multiple
store accounts per user account.</strong> As a future addition, it might
be possible to associate more than one store account with each user
account. For example, this model would allow Adam to link both S123 and
S456 to U1000. This matches the model of accounts used by Google Play on
Android, with Google accounts corresponding to our store accounts.

We do not assume that the store account ID is kept secret from the user:
for example, it is likely to be visible in URLs or cookies while
accessing the app-store.

The store account ID is considered to be confidential from the
perspective of most app-bundles, to minimize its privacy impact
(mitigating tracking/profiling of app users by app authors). If certain
app-bundles require access to the store account ID, we could offer a
permissions flag analogous to
[`android.permission.GET_ACCOUNTS`](https://developer.android.com/reference/android/Manifest.permission.html#GET_ACCOUNTS),
which controls the equivalent access in Android. However, the
conditional-access mechanism must be able to work with app-bundles that
are not allowed to know the store account ID.

### Device ID

We assume that each Apertis device has an opaque, unique *device ID*,
analogous to the serial number of a consumer electronics device or the
IMEI of a cellular phone, which never changes except in situations that
the OEM's business rules treat as "replacing the device" (for example,
if the main circuit board of the Apertis device ceases to work and a new
main board is placed in the same housing, it might be reasonable to say
that this is essentially a new device and should therefore have a new
device ID).

We do not assume that the device ID is kept secret from the user: for
example, it might be printed on the housing of the Apertis device, and
the user might use it to register with the app-store on a laptop/desktop
computer if their vehicle does not have direct Internet access.

Like the store account ID, the device ID is considered to be
confidential from the perspective of most app-bundles, to minimize its
privacy impact (mitigating tracking/profiling of app users by app
authors). If certain app-bundles require access to the device ID, we
could offer a permissions flag analogous to
[`android.permission.READ_PHONE_STATE`](https://developer.android.com/reference/android/Manifest.permission.html#READ_PHONE_STATE),
which controls the equivalent access in Android. However, the
conditional-access mechanism must be able to work with app-bundles that
are not allowed to know the device ID.

If the device has cellular connectivity with an IMEI number, the device
ID is assumed to be distinct from the IMEI, and access to it may be
controlled by different permissions flags.

### Vehicle identification number

We assume that each vehicle with an attached Apertis device has an
opaque, unique identifier, the
[VIN](https://en.wikipedia.org/wiki/Vehicle_identification_number)
(vehicle identification number, chassis number). This never changes. It
is permanently applied to the vehicle (engraved in the chassis), and can
also be retrieved by the Apertis device in a way that cannot be faked by
an attacker.

We do not assume that the same Apertis device will necessarily remain in
a vehicle for the entire lifetime of the vehicle.

For the use-cases where OEM-defined paid services are linked to the VIN,
we assume that access to the digital representation of the VIN is
trusted, i.e. an attacker (for example the car's owner) cannot cause a
faked VIN to be received by the Apertis device.

We assume that it is acceptable to require out-of-band communication
with the app store whenever the association between Apertis devices and
VINs changes, for example if the user buys a faster/more capable Apertis
device for installation in their existing car, or if the Apertis device
fails or is damaged and is replaced by a different unit of the same
specification.

We assume that the VIN is not kept secret from the user: in particular,
it is sometimes visible from outside the vehicle, and frequently appears
in vehicle registration paperwork.

Like the store account ID, the VIN is considered to be confidential from
the perspective of most app-bundles, to minimize its privacy impact
(mitigating tracking/profiling of app users by app authors). If certain
app-bundles require access to the VIN we could offer a permissions flag
analogous to
[`android.permission.READ_PHONE_STATE`](https://developer.android.com/reference/android/Manifest.permission.html#READ_PHONE_STATE),
which controls the equivalent access in Android. However, the
conditional-access mechanism must be able to work with app-bundles that
are not allowed to know the VIN.

### App developer account

We assume that each [ISV]( {{< ref "/glossary.md#isv" >}} ) wishing to work on
app-bundles must register a *developer account* with which they will
submit the finished app-bundle for approval by the app-store curator.
(Larger ISVs might have separate developer accounts for different
business units.) Each of their app-bundles is associated with that
developer account via its [bundle ID]( {{< ref "/glossary.md#bundle-id" >}} ), a
[reversed domain name]( {{< ref "/glossary.md#reversed-domain-name" >}} ): the
developer should register one or more reversed domain names for their
development, and should be given the exclusive ability to create
app-bundles "below" those names (for example, Collabora Ltd. might
register `com.collabora` and `uk.co.collabora`, and would be the only
ISV allowed to release `com.collabora.ShoppingList`). The app-store
curator may verify ownership of those reversed domain names, and we
recommend that this is done.

We assume that the app store will give the ISV the ability to associate
one or more [store accounts]( {{< ref "/glossary.md#store-account" >}} ) and one
or more Apertis devices with their developer account.

## Clocks

We assume that the device has access to a reasonably accurate real-time
clock measuring time since an "epoch" (reference point) based on UTC,
which continues to run while the device is switched off, and is
automatically synchronized to a reasonable approximation of the actual
time during operation. This could be done using the Internet (NTP,
tlsdate or similar), a cellular network, GPS, digital radio or any other
trusted out-of-band mechanism.

We assume that the user is unable to alter the internal clock, and can
only change its display/interpretation (i.e. the time zone, and
therefore the local time). For example, if the Apertis device knows from
time-synchronization that the date and time is 2016-01-15 16:55:42 UTC,
the user may configure the device so that clock display, alarms, etc.
treat that value as a local time of 2016-01-15 17:55:42 +0100 (the
equivalent time in Berlin) or 2016-01-15 11:55:42 -0500 (the equivalent
time in New York), but the ability to change the internal clock to (for
example) 2016-01-07 or 16:55:07 is not required.

  - *This is how timing normally works for unprivileged users on Linux
    systems, so this only requires that we avoid giving CAP_SYS_TIME
    capabilities to processes outside the TCB, and avoid providing APIs
    by which a process not in the TCB can instruct a process in the TCB
    to change the time.*

We assume that the owner of the device is unable to interfere with the
real-time clock or its automatic synchronization in order to set the
clock to an incorrect time/date.

## Access to the filesystem

We assume that the user of the device cannot directly read its
filesystem from the underlying storage. This assumption could be
satisfied either by physical access being outside the scope of the
threat model, or by encrypting the filesystem device using an encryption
key stored somewhere that is not feasible to access, such as [sealed
storage](https://en.wikipedia.org/wiki/Trusted_Computing#Sealed_storage)
provided by a TPM (Trusted Platform Module). This assumption is
necessary if we wish to have anything on the device that cannot be
copied by the owner of the device/vehicle.

## Internet access

We assume that the user of the Apertis device has Internet access,
either through the vehicle or via a desktop/laptop computer that can
write to USB mass storage devices such as flash drives.

We do not assume that the Apertis device itself will ever have direct
Internet access: in general, these conditional access scenarios should
continue to work without it. However, individual scenarios may work
poorly or not at all without direct Internet access, where noted in the
individual scenarios.

## App-bundle integrity

We assume that the unencrypted file encapsulating the app-bundle
contains digital signatures that prove that it has been approved by the
app-store as of a particular date, and has not been modified since it
was approved. In other words, the scope of this design is only to
protect the business models of the app-store and ISV from the device
owner; protecting the device owner from a maliciously created or
modified app-bundle is assumed to have been solved elsewhere (this is in
the scope of the [Applications]( {{< ref "applications.md" >}} ) concept
design).

Similarly, if a trust path from the ISV to the app-bundle is desired
(protecting the ISV and device owner from a malicious app-store
curator), this is assumed to be outside the scope of this particular
design.

## Trips

The OEM may define a way to determine whether the vehicle is
conceptually in the same trip (journey) as it was at an earlier time.
For example, the time from engine start to engine stop might be
considered to be a trip, a short time period with the engine stopped
might be considered not to interrupt a trip, or the [automotive
domain]( {{< ref "/glossary.md#automotive-domain" >}} ) might be treated as the
"single source of truth".

If the OEM does not define this, then we will assume that each run of
the Apertis device (infotainment domain), from bootup to shutdown, is a
trip.

This influences some of the requirements below.

# Requirements

## Unrestricted app-bundle

(This is a baseline so that the other scenarios can be compared with it;
there is no real conditional access here.)

An [ISV]( {{< ref "/glossary.md#isv" >}} ) publishes an [application
bundle]( {{< ref "/glossary.md#store-application-bundle" >}} ) in an app store.
This ISV is not interested in limiting access to this particular
application bundle in any way: perhaps it was developed as a marketing
tool and is published to everyone at no cost, or perhaps the app-store
curator or ISV has decided that the customer-service cost of imposing
strong conditional access exceeds the value of this particular
app-bundle.

(Please note that if an app-bundle is available at no cost, but is
conditional on some other factor - for example the OEM, the VIN or the
user - then that is not an unrestricted app-bundle, and falls into one
of the other scenarios below.)

A user downloads and installs this app-bundle.

  - If the vehicle has an Internet connection, the user can download,
    install and use this app-bundle.
  - The user can download one or more files that encapsulate this
    app-bundle via a home PC, copy it to a USB thumb drive or similar
    removable mass-storage, and install it in the vehicle from there,
    without the vehicle having Internet access.
  - The app-bundle can continue to be used while no Internet connection
    is available.
  - This app-bundle never expires, and can continue to be used
    indefinitely.
  - *Non-requirement:* If the user downloads the files to USB storage
    and attempts to install them in multiple vehicles, it is acceptable
    for this to work in this specific situation, but it is not required.
    It would also be acceptable for the required files to change for
    each vehicle or for each store account.

A new version of the same app-bundle becomes available. It is still
unrestricted.

  - If the vehicle has a direct Internet connection, the user can
    upgrade to this new app-bundle "over the air".
  - The user can download one or more files that encapsulate this
    app-bundle via a home PC, and upgrade to it using a USB storage
    device.

## App-bundle licensed to a store account

An ISV publishes an application bundle for purchase on an app store. The
ISV and/or app store curator has chosen to link this bundle's licensing
to a [store account]( {{< ref "/glossary.md#store-account" >}} ), so that any
vehicle linked to that store account can have that app-bundle. For
example, this matches the model used by services like [Google Play (for
Android apps, music, videos,
etc.)](https://www.androidpit.com/use-multiple-devices-on-google-account)
and [Steam (for games and other
apps)](https://support.steampowered.com/kb_article.php?ref=8963-EIKC-3767).

A user has a store account which they have associated with one or more
vehicles in some way, for example by entering the relevant vehicles'
into the store's web interface on a desktop or laptop computer, or by
entering their store account username and password into the vehicle's
HMI while it has Internet access. The user purchases the app-bundle,
which in this scenario gives them permission to use the app-bundle on
all of their vehicles.

For this scenario, assume that the app-bundle is a one-time purchase
which remains valid forever (see below for details of what would differ
for a [Subscription-based
app-bundle](#subscription-based-app-bundle) or
[Time-limited app-bundle](#time-limited-app-bundle)).

The availability and purchase price of the app-bundle might vary
depending on characteristics of the store account, for example the
country and currency with which it is registered.

  - The store curator can choose the business rules for associating
    vehicles and/or devices with store accounts (for instance limiting
    the number of vehicles per account, the number of accounts per
    vehicle and/or the frequency of changes).
  - The purchase and download of the app-bundle can either be done
    directly on the Apertis device, or via USB storage, as above.
  - The app-bundle can continue to be used while no Internet connection
    is available.
  - If the vehicle or device is multi-user, the OEM chooses whether the
    user's store account is associated with a particular [user
    account]( {{< ref "/glossary.md#user-account" >}} ), or part of system-wide
    configuration.
      - If the store account is associated with a particular user
        account, the OEM chooses whether an app-bundle purchased by one
        user of the device is automatically available to all other user
        accounts on the same device (similar to games on the PlayStation
        3, for example), or whether the app-bundle is only available to
        the user account matching that store account (similar to Steam).
  - *Non-requirement:* If the user downloads the files to USB storage
    and attempts to install them in multiple vehicles that are all
    linked to their store account, it is acceptable for this to work,
    but it is not required (it would be acceptable for the required
    files to change for each vehicle).

The user attempts to give a friend a copy of the app-bundle without the
friend paying for it.

  - If the user downloads the files to USB storage and attempts to
    install them in a vehicle that is *not* linked to their store
    account, the app-bundle must not be installed or usable on that
    vehicle.
  - It must not be feasible to extract the contents of the app-bundle
    (for example music tracks) by unpacking the downloaded files
    manually.

Like an [Unrestricted
app-bundle](#unrestricted-app-bundle), upgrades should work
similarly to installation.

  - The upgraded version can be downloaded directly onto the device, or
    copied via USB storage.

## App-bundle licensed to a vehicle

An ISV publishes an application bundle for purchase on an app store. The
ISV and/or app store curator has chosen to link this bundle's licensing
to a specific vehicle, not to the store account or the Apertis device.
For example, this would be appropriate if access to the app-bundle is
sold as "part of the vehicle" for an extra cost: in the same way that a
car buyer can pay extra for a more powerful engine or for metallic
paint, they might pay extra for a more advanced navigation app.

A user has a store account which they have associated with one or more
(vehicle, device) pairs in some way, as in the [App-bundle licensed to
a store account](#app-bundle-licensed-to-a-store-account)
scenario. The user purchases the app-bundle for use on a specific
device.

For this scenario, assume that the app-bundle is a one-time purchase
which remains valid forever (see below for details of what would differ
for a [Subscription-based
app-bundle](#subscription-based-app-bundle) or
[Time-limited app-bundle](#time-limited-app-bundle)).

The availability and purchase price of the app-bundle might vary
depending on characteristics of the vehicle. In particular, the cost
might be zero for certain vehicles, for example if a car manufacturer
makes a particular piece of navigation software available at no extra
cost for cars in their premium range, or for their entire range.
Conversely, the app-bundle might not be offered at all for certain
vehicles: for example, it might be exclusively available on premium cars
from a certain manufacturer.

  - The purchase and download of the app-bundle can either be done
    directly on the Apertis device, or via USB storage, as above.
  - The app-bundle can continue to be used while no Internet connection
    is available.
  - If the user downloads the files to USB storage and attempts to
    install them on a device that is installed in a different vehicle,
    the app-bundle must not be installed or usable on that device.
  - If the user moves the Apertis device from one vehicle to another,
    the app-bundle must not be launchable on that vehicle until/unless a
    separate license is purchased.

Suppose the user's vehicle is damaged and "written off", for example due
to an accident. The user obtains a new vehicle.

  - The store curator can choose the business rules they will follow in
    this case, perhaps influenced by warranty lifetimes. If the store
    curator wishes to provide the same app-bundles on the replacement
    vehicle, we anticipate that this would be done in the store backend,
    for example by having a [Trusted
    dealer](#trusted-dealer) confirm that the device has
    been moved to a replacement vehicle, then issuing new app-bundle
    licenses for the replacement vehicle at no cost to the user.

Suppose the Apertis device in the vehicle is replaced, either due to
hardware failure or accidental damage, or as an upgrade to a
higher-specification Apertis device.

  - The user can register the new (vehicle, device) combination with
    their app-store account.
  - The user can re-download, install and use the same app-bundles that
    were already licensed to the vehicle, either directly or via USB
    storage.

Suppose instead that the user sells their vehicle, with the same Apertis
device remaining installed. The user performs a data reset to ensure
that all personal data gets deleted.

  - The new vehicle owner can associate the Apertis device with their
    new or existing app-store account.
  - There are multiple options for what should happen during a data
    reset. All of the following options are required to be supported,
    but each OEM variant (Apertis-based operating system) is only
    required to support one of these options. (This is also relevant to
    [Application Layout]( {{< ref "application-layout.md" >}} ).)
    1.  One option is that the license to each app-bundle associated
        with the vehicle is revoked, and all the app-bundles on the
        device are deleted. If the new owner wishes to use those
        app-bundles, they are required to re-purchase them.
    2.  The other possibilities all assume that for each app-bundle
        licensed for use on the vehicle, the license for that app-bundle
        is transferred to the new owner. There are several options for
        what happens to the contents of the device:
        1.  Whatever app-bundles happen to have been installed on the
            device at the point of handover remain installed for the new
            owner. The new owner can subsequently re-download those
            app-bundles for the same vehicle; the new owner can also
            download the other app-bundles whose licenses were
            transferred.
        2.  All app-bundles on the device are deleted. The new owner can
            subsequently download the app-bundles whose licenses were
            transferred.
        3.  All app-bundles purchased for this device, even those that
            were not actually installed on the device at the point of
            handover, are automatically downloaded after handover.
            (Clearly, this might require Internet access.)
  - If any app-bundles licensed to the vehicle were not installed prior
    to the data reset, or were uninstalled during the data reset, the
    new vehicle owner can download and install them in the same ways
    that the previous vehicle owner could, without additional payment.
  - The new vehicle owner can continue to use and upgrade the
    app-bundles.

Suppose instead that the user sells their vehicle, removing the Apertis
device. The new vehicle owner obtains a new Apertis device.

  - This is essentially a combination of the two previous use-cases.
  - The new vehicle owner can associate the Apertis device with their
    new or existing app-store account.
  - If the OEM option that the licenses are not revoked was chosen, the
    new vehicle owner can download and install any app-bundles that were
    licensed to the vehicle, without additional payment.

Suppose the device is easily removable, and the user moves it to a
different vehicle temporarily or permanently.

  - Any app-bundles that are licensed to the first vehicle cannot be
    launched on the second vehicle, unless the user obtains and installs
    a separate license for the second vehicle.

Suppose the user attempts to give a friend a copy of the app-bundle
without the friend paying for it.

  - If the user downloads the files to USB storage and attempts to
    install them on a different vehicle, the app-bundle must not be
    installed or usable on that vehicle.
  - It should be possible for an optimized user interface to detect this
    and redirect the friend to an appropriate part of the app-store, for
    example offering the same app-bundle for purchase, or offering a
    trial version of the same app-bundle.
  - It must not be feasible to extract the contents of the app-bundle
    (for example music tracks) by unpacking the downloaded files
    manually.

## App-bundle licensed to a device

*We anticipate that per-store-account or per-vehicle are the more
mainstream options, and this is a lower-priority (more "niche") option -
it might be reasonable to have a design that can cover all of the
possibilities, but an initial implementation that omits the per-device
option.*

An ISV publishes an application bundle for purchase on an app store. The
ISV and/or app store curator has chosen to link this bundle's licensing
to a specific device (the infotainment unit installed in a vehicle), not
to the store account or the vehicle.

A user has a store account which they have associated with one or more
vehicles and one or more devices in some way, as in the [App-bundle
licensed to a store
account](#app-bundle-licensed-to-a-store-account) scenario.
The user purchases the app-bundle for use on a specific device.

For this scenario, assume that the app-bundle is a one-time purchase
which remains valid forever (see below for details of what would differ
for a [Subscription-based
app-bundle](#subscription-based-app-bundle) or
[Time-limited app-bundle](#time-limited-app-bundle)).

As with per-vehicle licensing, the availability and purchase price of
the app-bundle might vary depending on characteristics of the device. In
particular, the app-bundle might only be available for certain devices,
and the cost might be zero for certain devices.

  - The purchase and download of the app-bundle can either be done
    directly on the Apertis device, or via USB storage, as above.
  - The app-bundle can continue to be used while no Internet connection
    is available.
  - If the user downloads the files to USB storage and attempts to
    install them on a different different device, the app-bundle must
    not be installed or usable on that device.

Suppose the user's device is damaged by a hardware failure or accident,
and is no longer usable. The damaged device is replaced.

  - The store curator can choose the business rules they will follow in
    this case, perhaps influenced by warranty lifetimes and whether the
    repair/replacement was carried out by an authorized dealer who can
    be trusted to remove old license keys or otherwise cooperate with
    enforcing conditional access (a [Trusted
    dealer](#trusted-dealer)). If, based on their business
    rules, the store curator considers the new component or the new
    device to be a replacement for the old, they can issue new license
    keys allowing conditional-access apps that were linked to the old
    device to be used on the new device instead.

Suppose the device is easily removable, and the user moves it to a
different vehicle temporarily or permanently.

  - Any app-bundles that are licensed to the device remain installed and
    usable on the new vehicle.

Suppose the user buys a different Apertis device and arranges for an
authorized dealer to install it in their vehicle. For example, they
might choose to replace a low-end device with a more fully-featured
premium device with a higher specification, such as a faster CPU or more
memory. If the low-end device is still usable, the user might sell the
low-end device to another user, or might sell it back to the authorized
dealer (for re-sale to another user) as a part-exchange.

  - Any app-bundles that are licensed to the device are no longer
    available to the user on their new Apertis device.
  - Both of these results must be supported, with the choice between
    them depending on the app-store curator's business rules:
    1.  The new owner is not granted any licenses, the app-bundles are
        deleted from the low-end device, and the original owner is
        granted per-device licenses for the same app-bundles on the
        premium device instead
          - We anticipate that this result requires the cooperation of a
            [Trusted dealer](#trusted-dealer). It is
            unlikely to be possible to avoid the possibility of an
            untrustworthy dealer telling the app store that they have
            deleted the device-specific licenses from the old device
            (triggering issuance of licenses for the new device), but
            not actually doing so, resulting in the same app-bundle
            existing on two devices without any additional payment.
    2.  The original device owner's choice of per-device app-bundles on
        the low-end device follows the low-end device to its new owner
        (i.e. the new owner is granted a license for each of those
        app-bundles, and the original owner does not receive a
        corresponding license for their new device). There are three
        further possibilities which should all be supportable:
        1.  Whatever app-bundles happen to have been installed on the
            device at the point of handover remain installed for the new
            owner. The new owner can subsequently re-download those same
            app-bundles for the same device.
        2.  Another option is that the app-bundles on the device are
            deleted.
        3.  A third option is that all app-bundles purchased for this
            device, even those that were not actually installed on the
            device at the point of handover, are automatically
            downloaded after handover. Clearly, this might require
            Internet access.

Suppose the user attempts to give a friend a copy of the app-bundle
without the friend paying for it.

  - If the user downloads the files to USB storage and attempts to
    install them on a different device, the app-bundle must not be
    installed or usable on that device.
  - It must not be feasible to extract the contents of the app-bundle
    (for example music tracks) by unpacking the downloaded files
    manually.

## App-bundle licensed with multiple factors

It must be possible to combine the scenarios above, to obtain
app-bundles that require two or more of: a specified store account, a
specified vehicle, and a specified device. This is essentially
equivalent to the [App-bundle licensed to a
vehicle](#app-bundle-licensed-to-a-vehicle) and
[App-bundle licensed to a
device](#app-bundle-licensed-to-a-device) scenarios, except
for the use-cases where an existing device or vehicle becomes associated
with a new store account: in the other scenarios, the app-bundle remains
available to the device, whereas in this scenario, the device's access
to the app-bundle is revoked.

Similarly, the scenarios above describe factors that might influence the
purchase price for an app, but those factors could be combined (for
example an app-bundle that is cheaper or zero-cost if you have an
Apertis device from OEM A in a vehicle from manufacturer B).

## App-bundle licensed to a class of devices or vehicles

An app-bundle might be licensed to any/all devices or vehicles sharing
some common feature, for example all the devices with a particular OEM's
variant of Apertis, or all the vehicles of a specific manufacturer,
model or family of models.

  - It must be possible for the owner of a device/vehicle in the
    required class to obtain an app-bundle for installation on their
    device/vehicle.
  - *Non-requirement:* The same installation files are not required to
    be installable on other devices/vehicles of the same class (in other
    words, it is acceptable for the downloadable installation files for
    the same app to vary between owners, devices and/or vehicles).
  - The same installation files must not be installable on
    devices/vehicles not in the required class.
  - It must not be feasible to extract the contents of the app-bundle
    (for example music tracks) by unpacking the downloaded files
    manually.

We anticipate that this should be treated as a special case of the
scenarios above, so that whenever the user wishes to install the
app-bundle on a new device or vehicle, they would effectively purchase a
new license for the same app-bundle, with its purchase price reduced to
zero whenever business rules dictate that there should be no additional
cost.

## Time-limited app-bundle

An ISV publishes an app-bundle on a time-limited basis, for example
charging a flat fee for 1 year of access. The user buys access to this
app-bundle.

We assume for this scenario that the feature being controlled is access
to the app-bundle itself. See [Per-feature conditional
access](#per-feature-conditional-access) for the situation
where the app-bundle itself remains available, but individual features
expire when the subscription runs out.

  - The purchase and download of the app-bundle can either be done
    directly on the Apertis device, or via USB storage, as above.
  - The app-bundle can be licensed to a store account, a vehicle and/or
    a device, as above.
  - The app-bundle can be used without Internet access for the duration
    of its license validity.
  - When the app is launched, if the user's license is still valid, the
    application must launch.
  - The app-bundle should be able to ask the system how long its license
    lasts, so that it can warn the user when it is going to expire soon.

App-store curators or ISVs might have differing requirements for the
validity period. Suppose 365 days of access to an app-bundle are bought
on 2017-01-01, the app-bundle is installed on 2017-01-12, and it is
first used on 2017-01-23.

  - The validity period might expire on 2018-01-01, 365 days since the
    date of purchase.
  - The validity period might expire on 2018-01-12, 365 days since the
    date of installation.
  - The validity period might expire on 2018-01-23, 365 days since the
    first use.
  - All of these three options are required to be supported by the
    framework. Individual OEMs might only use a subset of them.

The app-bundle becomes unavailable for use shortly after its validity
period expires.

  - For simplicity, we recommend that a nominal lifetime of (for
    example) 365 days runs out 365×24×60×60 seconds after it begins,
    with factors such as time zones entirely ignored.
  - If the app-bundle's nominal lifetime expires while it is running,
    the app-bundle must continue to run until the end of the current
    [trip](#trips), or until specifically closed, whichever
    comes first. This is an automotive-specific requirement: not
    distracting the driver is considered to be a higher priority than
    conditional access, so the system should not arbitrarily terminate a
    running app-bundle while the driver is using it.
      - Similarly, if the app-bundle is placed in the background, and
        subsequently terminated by the application framework to save
        resources, then re-running it should be allowed until the end of
        the current trip (regardless of the resolution of the unresolved
        point below).
      - If the app-bundle's nominal lifetime has expired since the
        beginning of the current trip, but it was specifically closed by
        the user or was never launched during this trip, either of these
        behaviours is acceptable:
        1.  The user can still launch it for the duration of the trip.
        2.  The user cannot re-launch it.
        <!-- end list -->
          - <em id="future-specify-precise-expiry-behaviour">Possible
            future addition:</em> this could be specified in one
            direction or the other in a future version of this design.
  - When the app-bundle's lifetime expires and there is no longer any
    other reason to allow it to be launched, it remains installed, and
    the user's data is not deleted. However, the app-bundle cannot be

launched, and app-specific variable data cannot be accessed.

  - While the app-bundle is installed but cannot be launched, it can
    still be upgraded. This is so that, if the user purchases additional
    access, it is less likely that they will inadvertently launch an
    outdated version of the app (which might have a known security
    vulnerability or some other important bug) before they have been
    given the opportunity to upgrade it.
  - The user can uninstall the app-bundle, whether it has already
    expired or not. Uninstalling the app-bundle deletes all app-specific
    variable data, as usual. If uninstalled, the app-bundle cannot be
    reinstalled until a new license is purchased.

A user might try to subvert this system by continuing to use an
app-bundle after it has expired.

  - If the user uninstalls and reinstalls the app-bundle, access must
    still expire after the appropriate time, even if the reinstallation
    is done without Internet access and using the same USB storage as
    for the original installation (in other words, without the
    opportunity to contact the app store).
  - If the user rolls back the app-bundle to an old version, access must
    still expire after the appropriate time. If they upgrade it again,
    it must still expire after the appropriate time, even if the upgrade
    is done without Internet access and using the same USB storage as
    for the original installation.
  - If the user adjusts the clock so that the expiry date does not
    appear to be reached, access must still expire after the appropriate
    number of (real-time) days.
      - *Implementation note: with our assumption of a trusted
        [Clock](#clock), this can be achieved by ignoring
        the user-settable time zone, and ensuring that all expiry
        times/dates are processed using UTC, the same as the device's
        real-time clock.*
  - Replay attacks must not work: if the user repeatedly provides the
    same access token via USB storage, or cycles through a finite number
    of access tokens, the system must recognise these as invalid.

After the app-bundle has expired, suppose the user decides that the
app-bundle is worth re-purchasing, and purchases a new license.

  - The valid license should be represented by downloading a small
    licensing token; the content of the app-bundle should not be
    re-downloaded.
  - Old tokens that are no longer required should be deleted, to avoid
    filling device storage with an unbounded number of tokens.
  - If the user did not uninstall the app-bundle, then they regain the
    ability to launch it, and any app-specific variable data that
    remained on the system.
  - If the user uninstalled the app-bundle, then they can reinstall
    (with the same or a different version). Its app-specific variable
    data will not be restored.

Alternatively, the user might buy additional access to the application,
for example an additional year, before the previous license has run out.

  - Again, the extended license should be represented by downloading a
    small licensing token; the content of the app-bundle should not be
    re-downloaded.
  - The precise definition of the new license's lifetime is decided by
    the store curator's business rules.
  - We suggest that its expiry date should normally be the date of
    purchase, plus the advertised lifetime, plus the remaining unused
    lifetime of the old license if any: for example, if a license bought
    on 2017-01-01 would normally expire on 2018-01-01, and the user pays
    for an additional year on 2017-11-01, we suggest that this should
    normally result in a new license valid until 2019-01-01 and not
    2018-11-01. However, it is the app store curator's decision whether
    to do this or not.

## Subscription-based app-bundle

An ISV publishes an app-bundle with a recurring cost, for example
charging a monthly fee. The user subscribes to this app-bundle.

We anticipate that app-bundles will normally only be made available via
this model if they require at least intermittent Internet access for
their normal functionality. However, ISVs may use this model for any
app-bundle.

  - The app-bundle can be used without Internet access for a limited
    time. The ISV or store curator may choose this time based on their
    business rules. It is a trade-off between two competing factors:
      - if it is too long, the user could terminate their subscription
        and continue to use the app-bundle until the time expires;
      - if it is too short, the user might be prevented from using the
        app-bundle even though they are still paying the subscription,
        if they are unable to obtain a new licensing token sufficiently
        soon
  - If Internet access is available, the system should obtain a new
    licensing token at regular intervals to avoid unwanted expiry while
    Internet access is unavailable.
      - The precise interval between licensing token retrievals depends
        on the store curator and perhaps the ISV, and should probably
        also be influenced by the length of the subscription. For
        example, it might be considered reasonable to try to obtain a
        new token whenever the current token is halfway to expiring.
      - These new licensing tokens might have a "pull" model where the
        Apertis device periodically polls the app-store, or a "push"
        model where the app-store periodically sends tokens to the
        Apertis device, or a combination of the two.
  - Old tokens that are no longer required should be deleted, to avoid
    filling device storage with an unbounded number of tokens.

The app-bundle becomes unavailable for use shortly after its most recent
validity period expires. For example, this might happen because the user
cancels their subscription, or because an instance of the recurring
subscription charge is refused by their credit card issuer.

  - The requirements are the same as for a [Time-limited
    app-bundle](#time-limited-app-bundle).
  - In addition, if the Apertis device happens to have Internet access,
    it might become aware that the license has been revoked, either by
    polling the app-store (a "pull") or by being sent a revocation token
    (a "push").

A user might try to subvert this system by continuing to use an
app-bundle after it has expired.

  - The requirements are the same as for a [Time-limited
    app-bundle](#time-limited-app-bundle).
  - *Non-requirement*: because we cannot rely on the user to cooperate
    with a process that is detrimental to them, we cannot rely on the
    ability to receive license revocations as described above: if the
    Apertis device is prevented from communicating with the app store,
    it will never receive the revocation.

After terminating their subscription, the user might decide to resume
their subscription.

  - The requirements are the same as for re-purchasing a [Time-limited
    app-bundle](#time-limited-app-bundle) that has expired.

## Time-limited trial version

Suppose the ISV publishes a paid-for navigation app-bundle in the app
store, and in addition to the full version, they offer a free,
time-limited trial version (for example it might last for 30 days). A
user downloads the trial version.

We suggest that this is implemented as a time-limited app-bundle where
the first 30-day token can be "purchased" for no cost, but the app store
refuses to issue a second token to the same entity (store account,
vehicle and/or device as appropriate) without payment, and paid tokens
are longer-lasting or unlimited.

  - The download of the app-bundle can be done directly on the Apertis
    device, or via USB storage, as above.
  - The trial version can be licensed to a store account, a vehicle or a
    device, as above.
  - The combined system consisting of the app-store and the Apertis
    system must be able to prevent additional trial periods for the same
    entity (store account, device or vehicle as appropriate).
      - We suggest only enforcing this on the app-store side, so that
        the Apertis side does not rule out repeated promotions. For
        example, an ISV might wish to offer "free access to this app
        this month" on more than one occasion, with the promotion
        available to users who participated in a previous promotion.
  - The trial version can be used until it expires, as above.
  - The trial version cannot be used after it expires, as above.
  - The app-store curator or ISV must be able to specify that the
    limited trial starts the first time the application is launched, and
    not on a date fixed at the time of "purchase". This is specifically
    required for this scenario even if it is not supported for the other
    time-limited scenarios.
  - The trial version might have limited functionality when compared
    with the paid version.
      - This can be implemented by considering the differences between
        the trial version and the full version to be a [Per-feature
        conditional access](#per-feature-conditional-access)
        feature, and including that feature at no additional cost with
        any license for the full version.

(See [Per-feature conditional
access](#per-feature-conditional-access) for the scenario
where a trial version is not time-limited, which is outside the scope of
this scenario.)

## OS developer access

A developer working on the Apertis operating system (i.e. the upstream
Apertis open-source project) or an Apertis variant (for example for an
OEM) might need to modify an Apertis device so that it can execute
arbitrary code of their choice within the [trusted computing
base]( {{< ref "/glossary.md#trusted-computing-base" >}} ). It should be
possible to provision an Apertis device in a special "developer mode".

  - App-bundles compiled by the OS developer must be installable on an
    OS developer device. The developer must be able to carry out the
    app-store role themselves, for instance marking any app-bundle as
    having been validated, using the same cryptographic protocols that a
    real app-store would use, but using cryptographic keys controlled by
    the OS developer instead of cryptographic keys that are known only
    to the app-store.
      - This is necessary for development and testing. If the
        app-bundles were functionally different (for example, if they
        were not signed at all), then the OS developer would not be able
        to reproduce or solve bugs in the conditional access.
      - The use of cryptographic keys controlled by the OS developer is
        analogous to Android development using "test keys" instead of
        Google or OEM signing keys.
  - App-bundles from a real app-store must not normally be installable
    on an OS developer device.
      - This is because an OS developer must be able to modify all
        components in the TCB, including any services responsible for
        enforcing conditional access (for example a licensing service),
        the low-level libraries that those services rely on, and the
        kernel. With that level of access, they would be able to defeat
        any conditional access scheme; a user who somehow obtained an OS
        developer device would be able to bypass conditional access.
      - The developer might arrange with the app-store curator to
        receive access to versions of app-bundles that can still run on
        their device, if this is consistent with the app-store's
        business rules. For example, if [Unrestricted
        app-bundles](#unrestricted-app-bundles) are made
        available on a per-device basis, it would be valuable for the
        app-store to provide versions of zero-cost applications that OS
        developers can use; this could include commonly preinstalled
        apps such as web browsers, and/or special developer apps such as
        debuggers. If an authorized developer is under a suitable
        non-disclosure agreement with the app-store curator, the
        app-store curator might also be willing to provide certain
        normally-restricted app-bundles in an unrestricted form that can
        be re-signed and installed by the OS developer, for example so
        that the developer can test interoperability with them.
  - In addition to any special SDK or development images that might be
    available with additional tools, OS developer devices must be able
    to run images that are the essentially same as production (target)
    images for the same hardware, but with different cryptographic keys
    provisioned.
      - Otherwise, it would be impossible for an OS developer to debug
        issues that might be influenced by the differences between
        target and development/SDK images.
  - *Non-requirement:* It is anticipated that OS developers will use
    special-purpose hardware, therefore it is not necessary that an OS
    developer device can be re-provisioned as a production (consumer)
    device.

A user who is not an authorized developer might try to defeat
conditional-access schemes ("jailbreaking" their Apertis device) by
obtaining an OS-developer version of the operating system, for example
one made available by apertis.org.

  - Production devices must not accept developer versions as valid OS
    updates.
      - *For example, production OS images could be signed with
        cryptographic keys whose controller guarantees to only sign
        production-mode images that enforce conditional access, and
        never sign developer-mode images that do not; and the updater,
        and/or a facility analogous to [UEFI Secure
        Boot](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface#Secure_boot),
        could check for a signature from those keys.*

## App developer access

A developer working on an Apertis application (for example for an
[ISV]( {{< ref "/glossary.md#isv" >}} )) needs to be able to run the app-bundle
that they are developing.

  - *Non-requirement:* The developer is not required to be able to
    install app-bundles without registering a [Developer
    account](#developer-account).
  - After registering a developer account and associating the new app's
    [bundle ID]( {{< ref "/glossary.md#bundle-id" >}} ) with that developer
    account, the developer must receive the necessary information to be
    able to compile an app-bundle that can be installed on a device that
    is associated with their developer account, and has a store account
    also associated with their developer account.
      - For example, the app store could provide the developer with a
        conditional access license key per store account which allows
        unlimited use of that app-bundle (on a specific device or on all
        devices), and can be compiled into the app-bundle.
      - Compiling these app-bundles must not require the developer to
        obtain validation from the app-store curator.
          - This is so that the developer can develop and test an
            unfinished version of their app.
  - If this app-bundle does not require any special permissions flags,
    the developer must be able to install it on the device, again
    without validation by the app-store curator.
      - The developer may be required to carry out some special action
        on the device, similar to the ways in which on-device developer
        mode, and remote debugging by a specific PC, are protected in
        Android.
  - If this app-bundle requires special permissions flags, but those
    flags solely exist to protect the integrity, availability or
    confidentiality of the user's data, then the developer must be able
    to install it on the device. This may require on-device
    confirmation, either when the app is installed or when a special
    developer mode is activated.
  - If there is a special developer mode, performing a data reset must
    return the device to its initial "non-developer" state.
      - This is the same behaviour as on Android.
  - The developer must not be able to install app-bundles whose bundle
    IDs are not among those associated with their developer account.
      - If they could, then they might be able to extract confidential
        data from another developer's app-bundle, by overwriting the
        executable part of that app-bundle with their own version.

A user might try to defeat conditional-access schemes ("jailbreaking"
their Apertis device) by registering as an app developer and making use
of permissions flags that can bypass conditional access.

  - If the app-bundle implementation supports any permissions flags that
    could be used to break the conditional access model or the TCB, and
    the developer's app-bundle has any of those flags, then the device
    must not allow that app-bundle to be installed.
      - These flags, if they exist, must be reserved for applications
        that have been developed and tested using [OS developer
        access](#os-developer-access), and have been
        carefully validated by the app-store curator. We recommend
        source code auditing for such applications.

## Trusted dealer

We might need to trust certain authorized vehicle dealers, garages or
other sale/repair locations to participate in the licensing process. For
example, if an Apertis device with faulty hardware is replaced with a
working device of the same specification, the dealer might notify the
app-store that they have done this and destroyed/wiped the old device's
device key, giving the device IDs of the two devices and the [Vehicle
identification number](#vehicle-identification-number). If
the app-store trusts that assertion to be true, they could give the new
device access to all the bundles that were licensed to the old device,
at no extra cost, on the basis that the old device's device key has been
put out of use.

The scope of this design includes a protocol for communicating that
assertion from the trusted dealer to the app-store, and proving that it
really came from the trusted dealer.

<strong id="term-for-dealers">Unresolved:</strong> Is there a standard
automotive jargon term for these entities?

## Per-feature conditional access

An ISV could opt to allow individual features, services or items of
content in an app-bundle to have conditional access, with the app-bundle
as a whole remaining available at all times. For brevity, we refer to
these as "features" here. Use-cases for this include:

  - A streaming ("radio") app might have conditional access for
    individual "channels". For example, the app-bundle might be
    installable with access to a limited number of channels at zero
    cost, with additional app-store or in-app payments required for
    premium channels. Each channel could be modelled as a "feature".
  - A game might have conditional access for extra levels and other
    downloadable content. For example, the app-bundle might have a cost
    for the base game, with additional app-store or in-app payments
    required for DLC. Each DLC item could be modelled as a "feature".
  - A navigation app might not be available as a standalone purchase at
    all, but might have multiple map packs for different countries, with
    the app itself (and a license to launch it) bundled with the first
    map pack purchased. The map packs could be modelled as "features"
    subordinate to the app itself; including a free license for the app
    itself with each map pack, and not offering the app for sale
    separately, could be a matter of business logic in the server side
    of the app-store.
  - An app-bundle might be offered with both a free trial version
    (either a [Time-limited trial
    version](#time-limited-trial-version), or
    non-time-limited), and a paid version with additional functionality.
    The additional functionality could be modelled as a "feature".
  - *Non-requirement*: A music player with integration into a service
    such as iTunes might have conditional access for tracks, albums or
    both. However, this could result in managing hundreds or thousands
    of individual "features" for a complete media library, and it seems
    likely that a service such as iTunes would wish to have its own
    orthogonal model of accounts and item ownership. As a result, we do
    not recommend treating this as in-scope for per-feature conditional
    access.

General requirements for these:

  - It must be possible for each app-bundle to have more than one
    conditional-access token with different names or identifiers.
  - One of those conditional-access tokens must control whether the
    application can be installed and launched at all. Depending on the
    ISV's business rules, this might be set to never expire, with all
    further conditional access done inside the application. The rest of
    the tokens represent "features".
  - It must be possible to link these tokens to a store account, device
    or vehicle.
  - The "base app" and each "feature" must each be able to follow any
    supported time-limitation model, independently of one another. For
    example, a navigation app might be a one-off purchase with no time
    limit, but its map packs might require subscription.
  - The app-bundle must be able to obtain a list of its other
    conditional-access tokens and their expiry dates.
  - Only one level of "feature" is required; hierarchies are not
    required.
  - For some use-cases, a "feature" does not require an additional
    download. For example, DLC in games is often present in all
    installations and merely needs to be "unlocked", particular for
    purely cosmetic features.
  - For other use-cases, each "feature" would imply downloading a
    separate file or files. For example, map packs for navigation
    software are sufficiently large that they should not be provided
    until needed.
  - <strong id="online-feature-validation">Potential future addition:
    online feature validation.</strong> For high-value content, it might
    be possible for the application to trigger online re-validation of a
    particular conditional-access feature token or tokens, to confirm
    that a subscription has not been revoked.
  - <strong id="features-bound-to-different-entities">Possible future
    addition: features bound to different entities</strong> If the "base
    app" is licensed to a particular type of entity (for example a store
    account), additional features could conceivably be licensed to a
    different type of entity (for example a vehicle or a (store account,
    vehicle) pair). For example, an ISV might wish to sell their
    navigation app on a per-store-account basis, but sell a speed camera
    location add-on for that app at an additional cost per vehicle.
    However, this is not currently considered to be a requirement.

## Validating access to web APIs

Suppose an app-bundle uses a web service provided by the ISV; for
example, a navigation software developer might provide a web service
that can be queried to locate nearby traffic congestion. The ISV wants
to limit access to this web service, only allowing access by users who
have paid for a valid, non-expired subscription to the corresponding
app-bundle.

  - The app-bundle must be able to communicate with the web service and
    possibly the app-store in a way that proves to the web service that
    it is validly licensed for a particular app-bundle.
  - That communication must not reveal any information from the device
    or vehicle that would allow the entity operating the web service to
    circumvent conditional access for any other app-bundle. Ideally, it
    would not reveal any information that would allow the ISV to
    circumvent conditional access for the current app-bundle either.
  - Similarly, the communication with the web service and app store must
    not result in the app-bundle's credentials being made available to
    other app-bundles, in a way that would allow other app-bundles to
    impersonate the first app-bundle.
  - For privacy reasons, that communication should not reveal the
    [Store account ID](#store-account-id), [Device
    ID](#device-id) or [Vehicle identification
    number](#vehicle-identification-number) to the web
    service, unless they are specifically required for this transaction.
  - Conversely, the communication may reveal the store account, device
    ID and/or VIN to the app-store, provided that this is done via a
    connection with strong confidentiality (such as https).

## Conditional access to a theme

This scenario is not currently a requirement. It is described here for
comparison.

**Possible future addition:** Suppose an ISV publishes a [system
extension]( {{< ref "/glossary.md#system-extension" >}} ) containing a theme.
This does not contain executable code that is launched, but it affects
the operation of the system (because all applications load it, if
configured). It is made available in the same way as one of the above
scenarios.

  - The user can select the theme for use whenever they would be able to
    launch a similarly-licensed program.
  - The user cannot select the theme if its time expires, if applicable.
  - To avoid driver distraction, an expired theme should remain active
    until the vehicle is stopped.
  - If the user switches to a different theme, they cannot switch back
    to the expired theme.
  - If the user has not already switched to an unexpired theme, the
    system automatically switches to either the previously-selected
    theme, or a system default theme. This must be done at or before the
    next time the Apertis device is shut down.

## Conditional access to a preinstalled app

An OEM might arrange for a store app-bundle to be pre-installed on a
device, for example as part of a promotion, but unavailable for use
until it is purchased, either as a one-off purchase, an ongoing
subscription or a time-limited purchase.

  - The requirements are identical to what would happen if it had been
    purchased and installed, and then its subscription or time limit
    expired: it remains present on the device, and can be upgraded, but
    cannot be launched until it has a valid license, and cannot be
    reinstalled once installed.

An OEM might arrange for a store app-bundle to appear to be
pre-installed on a device, but in fact only provide a placeholder which
will trigger purchase (if required) and download when launched, similar
to the scenario above. For example, this would be appropriate if the
store app-bundle that the OEM wishes to promote is not ready for release
when the OS installation image is released to manufacturing, but it is
anticipated that it will be ready by the time consumers receive devices
with that OS installation image (similar to "day 1 DLC" on games
consoles).

  - The requirements are identical to the above. In addition, the
    app-bundle must not be required to have content, other than what is
    required for a menu entry (an icon and some minimal metadata).
  - *This could be implemented by having "version 0" of the app-bundle
    be a placeholder containing only metadata, shipping it without a
    valid license as above, and "upgrading" to version 1 (with real
    content) when a license is obtained.*

## Conditional access to a built-in app

This scenario is not currently a requirement. It is described here for
comparison.

An OEM might include a built-in app-bundle in their OS images that is
unavailable for use until it is purchased. Alternatively, the built-in
app bundle might be available for use, but have
[features](#per-feature-conditional-access) that are not
available, with or without an additional download required to activate
them.

## Conditional access to an OS extension

This scenario is not currently a requirement. It is described here for
comparison.

**Possible future addition:** Suppose an ISV publishes a [system
extension]( {{< ref "/glossary.md#system-extension" >}} ) containing executable
code, for example a Telepathy connection manager for a particular
real-time communications protocol, or a GStreamer plugin for a
particular audio or video format. It is made available in the same way
as one of the above scenarios.

  - The user can select or use the system extension whenever
    appropriate, for example to create an account for that real-time
    communications protocol, or to play a video in that format.
  - The user cannot select or use the system extension at any time when
    an app with the equivalent conditional access would not be available
    for launching.
  - If the OS extension expires or its subscription is terminated while
    it is currently in use, it must remain available until no more
    app-bundles are using it.
      - ''As a generic solution that does not rely on the licensing
        service being able to determine what "in use" means for this
        particular extension, we suggest keeping it available until the
        end of the current [trip](#trips).

For certain extensions, for example those based on loading library code
(such as GStreamer plugins), use of a system extension is only possible
if the app-bundle making use of the plugin can read the file containing
the executable plugin code. This implies that if a malicious app-bundle
author intends to bypass conditional access, it would be physically
possible for that process to copy it.

  - The terms and conditions of a [Developer
    account](#developer-account) should forbid copying
    system extensions and other system files into the application's
    private storage or sending them to the network, so that if this is
    detected, it can have appropriate legal consequences.
  - The platform should make some attempt to detect when this is done,
    either at runtime or during validation. However, OEMs and ISVs
    should note that it will not, in general, be possible to detect or
    prevent this if the malicious app-bundle author is careful, because
    the platform cannot distinguish between an encrypted or obfuscated
    copy of the plugin, and (for example) an encrypted or compressed
    database of application-specific data.
  - If a system extension mechanism is anticipated to require
    conditional access, its designer should balance performance
    considerations against the confidentiality needed to enforce
    conditional access. For example, communicating with a service or
    agent via inter-process communication does not have this concern.

## Platform upgrades and downgrades

The Apertis platform is designed to be upgraded, either over-the-air or
via USB storage devices. The [System Update and Rollback design
document]( {{< ref "system-updates-and-rollback.md" >}} ) also calls for a rollback
mechanism, to be used if an updated version is faulty.

If a particular version of the platform images for a device has a flaw
in its conditional access mechanisms, or an exploitable flaw which can
be used to subvert a component that needs to be trusted by the
conditional access mechanisms (such as the kernel), then a device owner
might attempt to use this to bypass conditional access. Suppose version
13 has such a flaw, which is fixed in version 14.

  - The app-store must not provide new app-bundles with non-trivial
    access restrictions to an OS image such as version 13 that is known
    to have flaws affecting its conditional-access, unless the OS image
    is the latest available and the app-store curator considers denial
    of service (inability to sell new app-bundles) to be a greater
    commercial risk than weakening conditional access.
      - This might be implemented by not allowing any new app-bundles to
        be installed on OS images that are not the latest version
        available, regardless of whether they are known to have
        conditional access flaws (which would also reduce the number of
        variables from a QA point of view).
  - If a device was shipped with platform version 14 or later, the
    device owner must never be able to install version 13.
  - If the device has ever been upgraded to version 14 or later, the
    device owner must not be able to downgrade to version 13.
      - As an exception, if the "last-known-good" platform version
        before the most recent upgrade was version 13, the device owner
        may be allowed to roll back to that version as required by the
        System Update and Rollback designs. This prevents the following
        scenario: suppose version 13 has a flaw which can bypass
        conditional access, but version 14 has an even more serious
        problem, such as inability to start up, a bug that has a safety
        impact, or a remotely exploitable security vulnerability. Users
        who were previously running version 13 should still be able to
        undo the upgrade to version 14 and return to version 13, until a
        new version 15 fixing both issues can be released.

## Transition between charging models

An ISV might change the terms under which they are willing to sell an
application bundle (assumed to be identified by its [bundle
ID]( {{< ref "/glossary.md#bundle-id" >}} )).

Giving the new version a new bundle ID, and ceasing to sell the old
bundle ID, is one way this could be addressed, but because we treat
app-bundles as "black boxes" and do not currently have any plans for a
mechanism by which com.example.NewApp can gain privileged access to
files "owned" by com.example.OldApp, it seems likely that the ISV would
wish to keep the same bundle ID.

  - An application might change its licensing model in ways that make it
    more open/lenient (allowing things that were previously forbidden),
    for example switching from a time-limited or subscription model to a
    one-off purchase with no expiry, or switching from per-(store
    account, vehicle) to per-store-account licensing. Depending on the
    app store's business rules and the relative costs of the old and new
    licenses, users who have paid for the old model might receive a free
    license under the new model, or they might have to pay for one (for
    example when their subscription runs out, they might not be able to
    continue to use the app unless they pay a one-off cost for permanent
    access).
  - An application might change its licensing model in ways that make it
    more closed/strict (forbidding things that were previously allowed),
    for example switching from a one-off purchase to a time-limited or
    subscription model, or switching from per-store-account licensing
    (with unlimited devices and vehicles) to per-(store account,
    vehicle) licensing. Depending on the app store's business rules,
    users who have paid for the old model might continue to have access
    under the old model, or they might be made to switch to the new
    model.
  - An application might change its licensing model in ways that are
    ambiguously more or less open (allowing some things that were
    previously forbidden but simultaneously forbidding some things that
    were previously allowed), for example switching from per-device to
    per-vehicle licensing. Again, depending on the app store's business
    rules, users with access under the old model might retain it, or be
    moved to the new model, or they might get access to both.

Requirements:

  - The app store may issue new licenses to users at any time, according
    to its business rules. The next time the user downloads anything
    from the app-store, any updated license tokens for all of their
    app-bundles are included.
  - Any license token that allows a strict subset of what another
    license token does should be discarded, to free the space that it
    occupied.
      - For example, if the user has a permanent license for store
        account S123 and device ID 654, and receives a new permanent
        license for store account S123 on all devices, then the
        device-limited license token should be discarded.
      - Similarly, if the user has a 1-year license token for store
        account S123 and device ID 789, and receives a new permanent
        license for store account S123 and device ID 789, the
        time-limited token should be discarded.
  - If the terms of an application bundle change in a way that is
    unfavourable to the user (for example switching from
    per-store-account to per-(store account, device) licensing), or that
    is neither "strictly better" nor "strictly worse" for the user (for
    example switching from per-device to per-vehicle licensing), and the
    device discovers that the new terms exist, it must enforce them.
      - This should be approached carefully from a business, legal and
        public-relations point of view.
      - If this is required, we recommend that it should be explicit:
        new tokens should normally be installed alongside old. If a new
        token must revoke old tokens, it should specifically indicate
        what it revokes, perhaps by identifying the old tokens by a
        sequence number or a cryptographic hash.
      - Note that we cannot prevent a user from keeping their device
        offline if they anticipate that a subsequent update will be
        detrimental to them.
      - If a revoked license token is subsequently presented to the
        device again (perhaps via a USB storage device), it must be
        ignored.

# Unresolved assumption/requirement questions

Unresolved questions about the [Possible future
additions](#possible-future-additions) are not listed here.

  - [Is there a standard jargon term for what this document calls
    "trusted dealers"?](#term-for-dealers)

# Possible future additions

None of these features are required to be supported at this time. Some
are described in more detail above; others are only described here.

  - [Multiple store accounts per
    user](#multiple-store-accounts)
  - [Conditional access to a
    theme](#conditional-access-to-a-theme)
  - [Conditional access to an OS
    extension](#conditional-access-to-an-os-extension)
  - [Online feature validation](#online-feature-validation)
  - [Features bound to different
    entities](#features-bound-to-different-entities)
  - [Precise expiry
    behaviour](#future-specify-precise-expiry-behaviour)
  - **Limited app-store without conditional access:** In principle it
    would be possible to support installation of an [Unrestricted
    app-bundle](#unrestricted-app-bundle) on Apertis-based
    operating systems where conditional access is not required.
  - **Location-based access:** An ISV might wish to have location-based
    conditional-access, for example an app-bundle that is licensed to be
    used in Europe but not America.
  - **Discontinuous time restrictions:** An ISV might wish to have time
    restrictions that are not a simple continuous time interval from
    issue until an expiry date, for example an app-bundle that requires
    an additional subscription to be used at weekends.

# Proposed implementation

{{% notice warning %}}
These notes on proposed implementations are not up-to-date with the current
view of the requirements.
{{% /notice %}}

## Security model

The properties we are protecting are:

  - confidentiality of the contents of app-bundles
  - integrity of the information that the app manager uses to decide
    whether an app-bundle can be launched

The attacker is a user of Apertis devices who aims to read the
DRM-protected content (such as music) contained in the app-bundle, or to
induce the Apertis system to allow them to launch the app-bundle when it
should not.

This document does not attempt to address the integrity of app-bundles.
We recommend addressing this separately: for example, the [Access
token signing key](#access-token-signing-key) must be kept
online, whereas it might be possible to sign app-bundles for integrity
with a key that is kept offline, which would reduce its exposure.

## Cryptographic keys

**Unresolved:** We do not currently have concrete recommendations for
cryptographic algorithms or key sizes. The recommendations here should
take into account that the expected lifetime of a car is long, and
algorithms/key sizes currently believed to be adequate to resist any
realistic attacker will not necessarily remain adequate indefinitely.

### Access token signing key

The *access token signing key* is an asymmetric key-pair (public and
private key) used for digital signatures (for example RSA or ECC). The
private half must be available to the app-store, and nowhere else; it
must be kept constantly available (i.e. online) so that it can produce
new access tokens on-demand. The public half must be available on all
Apertis devices.

A signature from this key signifies that the [Access
token](#access-token) was issued by the app-store; Apertis
devices that are configured to trust this key will accept this signature
as evidence that they may use the application named in the access token,
subject to the other parameters in the access token.

For key agility, we recommend maintaining multiple access token signing
keys that are all equally trusted by Apertis devices, for example with
all but one of the private keys kept offline; for algorithm agility, it
would be possible for those multiple keys to use different digital
signature algorithms. These access token signing keys could have an
expiry date to limit the impact of a compromise.

*Impact of compromise:* If the private half of this key becomes
available to an attacker, then that attacker can give arbitrary devices
permission to launch any application (in other words, conditional access
is defeated) until those devices consider that key to have expired.

### Device key

The requirement that a user cannot extract content from a downloaded
app-bundle, but the Apertis device can, implies that each Apertis device
must be provisioned with some secret not available to the user (the
*device key*), which is used to decrypt the app-bundle.

We recommend that the manufacturer generates a unique asymmetric
key-pair consisting of a public and private key, for an asymmetric
crypto-system such as RSA or ECC, and uses that as the device key. Each
device needs to be provisioned with a unique [Device
ID](#device-id) (serial number) in any case, so no
additional effort is needed to provision the devices with unique device
keys at the same time.

The public half of the key-pair must be stored by the app-store, along
with the corresponding device ID.

The private half of the key-pair must be stored on the device, and
nowhere else. The reason for this recommendation is also the reason why
we use asymmetric cryptography here. If private or symmetric keys were
stored by the app-store, then the app-store would be a significant
target for attack: any attacker able to obtain private device keys would
be able to decrypt any app-bundle intended for any device.

We recommend that no two Apertis devices share a device key. In theory
it would be possible to share a device key between a class of identical
devices, but again, this would make an attack more attractive by
increasing the potential gain by an attacker: obtaining one device key
would allow decryption of any app-bundle intended for any device that
shared that key.

*Impact of compromise:* If the private half of the device key becomes
available to an attacker, then that attacker can obtain the [Ephemeral
encryption key](#ephemeral-encryption-key) for any encrypted
app-bundle that was validly available for installation on the device
with that key, and therefore can read the contents of that app-bundle.

### Session key

Each time the content of a restricted app-bundle is made available for
download for one or more devices, the app store generates a new
cryptographically random symmetric encryption key, the *session key*.
This is then encrypted using the public halves of one or more device
keys, such that it can only be decrypted by the devices holding the
private halves of those keys: this is the "[hybrid
cryptosystem](https://en.wikipedia.org/wiki/Hybrid_cryptosystem)" design
commonly used to encrypt a message for multiple recipients, for example
in OpenPGP and PKCS\#7.

*Impact of compromise:* If the session key becomes available to an
attacker, then that attacker can decrypt that particular copy of a
restricted app-bundle and read its contents.

## Implementation of download and installation

We recommend representing a downloaded app-bundle as follows:

  - generate a new [Session key](#session-key)
  - use the session key to encrypt a clear-text version of the
    app-bundle (assumed to already contain whatever ISV and/or store
    signatures are needed for [App-bundle
    integrity](#app-bundle-integrity), which is out of scope
    here), resulting in an *encrypted bundle*
  - for each authorized device, sign a file containing at least the
    following, resulting in a *signed access token*:
      - the bundle ID of the bundle to be licensed
      - **Unresolved:** Should this also include a cryptographic hash of
        the (encrypted or clear-text?) app-bundle, to bind the access
        token to a specific app-bundle implementation? This would make
        the access token version-specific, such that a new app-bundle
        version would always need to come with a new access token.
      - timestamp (seconds since a fixed "epoch" time, in UTC) at which
        this access token was generated
      - timestamp (seconds since a fixed "epoch" time, in UTC) at which
        this access token will expire, or an indication that it will
        never expire
      - the entity holding the license (store account ID, vehicle ID or
        device ID)
      - if the entity holding the license is a store account ID or a
        vehicle ID: the device ID for which this particular access token
        will be encrypted
  - for each access token, encrypt the signed access token and the
    [Session key](#session-key), using the corresponding
    *device key*, resulting in an *encrypted access token*
  - send the *encrypted bundle* and one or more *encrypted access
    tokens* to the user, either as separate files, a file for the
    encrypted bundle and a second file containing all encrypted access
    tokens, or a single file containing everything

**Unresolved:** In the current design we are encrypting the access
tokens, which has the effect of ["binding" the signature to the
encryption](https://theworld.com/~dtd/sign_encrypt/sign_encrypt7.html).
Are we gaining any desired security properties from doing so, or would
it be sufficient to send an encrypted session key along with a signed
but unencrypted access token? Does an attacker gain anything from the
ability to read access tokens?

For an [App-bundle licensed to a store
account](#app-bundle-licensed-to-a-store-account), if a
user's store account is associated with multiple Apertis devices, and
the user asks to download an app-bundle for installation via USB
storage, then they should receive an encrypted access token for each of
their devices, each containing the same value of the session key, so
that they can all decrypt the same encrypted bundle. The session key may
either be generated once for each revision of a bundle, or once for each
request to download files from the store. A different session key should
be used whenever the encrypted content differs, so that compromise of a
session key would not allow decryption of unrelated encrypted content.

Similarly, for an [App-bundle licensed to a
device](#app-bundle-licensed-to-a-device) or an
[App-bundle licensed to a
vehicle](#app-bundle-licensed-to-a-vehicle), any user whose
store account is associated with the relevant Apertis device should be
able to download an encrypted access token for that device, or for the
device installed in that vehicle. Only the relevant device will be able
to decrypt the resulting encrypted access token.

The value of the session key should not be stored by the Apertis device,
and can be discarded after installation. The *signed access token*,
including its signature, should be kept (see [Running an
application](#running-an-application)).

For [unrestricted app-bundles](#unrestricted-app-bundle),
the generation of the session key and all encryption steps may be
skipped if desired. For symmetry, we suggest that if unrestricted
bundles are unencrypted, they should still come with an unencrypted
signed access token, with:

  - the bundle ID of the bundle to be licensed
  - the timestamp at which the access token was generated (it can be a
    single access token for everyone)
  - an indication that this access token is valid for everyone
  - an indication that it will never expire
  - no store account IDs, vehicle IDs or device IDs

but without including a session key.

Alternatively, unrestricted bundles could be encrypted with a session
key in the same way as restricted app-bundles, and the app store could
simply provide access tokens for all the devices associated with any
store account at no cost to the user.

## Implementation of running an application

The application framework should store signed access tokens in a
directory that can be written by the application installer, and read by
the application launcher (Canterbury). For example, they could be placed
in directories named `/var/Applications/$bundle_id/tokens/` where
`$bundle_id` is the bundle identifier. The [Session
key](#session-key) does not need to be stored and should be
discarded after installation.

All bundles' tokens directories must be readable by all users'
application launcher processes. We suggest that they should be owned by
`root` with the standard `rwXr-Xr-X` permissions (0755 for directories,
0644 for files containing tokens). We suggest that the signed tokens
should be stored unencrypted, for simplicity: nothing is lost by not
encrypting them, because any process that does not have a legitimate
need to read them will be prevented from doing so by AppArmor profiles.
If they were encrypted, then any process that has a legitimate need to
read them would also need to be allowed to read a key that can decrypt
them, for example the session key, which would merely move the problem
from "protect the bundle contents with AppArmor" to "protect the
decryption key with AppArmor"; AppArmor is still part of the TCB either
way.

To allow an application bundle to remind a user that its time limit will
soon expire, we suggest that the tokens should remain unencrypted, and
each bundle's AppArmor permissions should allow it to read its own
tokens directory. This would also allow for [Per-feature conditional
access](#per-feature-conditional-access) if desired.

The application manager should not allow launching an application unless
its access tokens are consistent with the vehicle ID, device ID, etc.
that are current at the time of launching.

The version of the signed access token that is stored should include the
signature, so that the application manager can verify that the timestamp
has not been modified.

**Unresolved:** Is there any realistic threat model where the attacker
could modify the timestamp but could not modify the code that will
validate it? (But the cost of keeping the signature is negligible, so we
should keep it anyway, even if there's no point.)

## Implementation of time-limited access

The timestamps in a signed access token are timestamps, not relative
times (so for example a token might represent "you may use
com.example.Game from 2016-01-01 00:00:00+0000 to 2016-01-31
00:00:00+0000", but never "you may use com.example.Game from now until
now + 30 days"). This prevents replay attacks: for example, if a token
represented "now to now + 30 days", a user could place it on a read-only
USB storage medium and present it to the Apertis device repeatedly,
potentially receiving a different intepretation for the meaning of "now"
every time.

To have a precise definition for whether a time has expired, we
recommend that the timestamps in the signed access token are defined to
be UTC (not influenced by the time zone of the server, user, app-store
curator or ISV). We also recommend that the real-time clock is also
represented as UTC internally, and conversion from UTC to the user's
configured time zone is only done when the time is displayed (this is
how real-time clocks in Linux normally work, and is mentioned here for
completeness).

When "refreshing" an installation (for example to renew the lifetime of
a bundle, convert a trial version into a paid version, or ensure
continued access to a subscription-based bundle), we recommend that the
app store generates a new *signed token* with updated timing information
and encrypts it using the device key. If the store account is associated
with multiple devices and the user is downloading for installation via
USB storage, the app store should generate a signed token for each
device, and provide them in a single file or several adjacent files.
There is no need to include a session key unless the user is also
downloading an encrypted app bundle; if the.

**Unresolved:** As with the [Implementation of download and
installation](#implementation-of-download-and-installation),
is there any security benefit to the encryption here? If there is not,
then we do not necessarily need to encrypt to the device key at all.

If an old access token is superseded by a new access token with a later
expiry time, the old access token should be deleted after the new token
has been validated.

As previously noted, we recommend that a [Trial
version](#trial-version) is simply implemented as a
[Time-limited app-bundle](#time-limited-app-bundle), with
the app store's business logic set up such that it will allow one
short-term access token per store account, device or vehicle to be
"purchased" at no cost; this requires a small amount of additional logic
in the app store, but no new logic on the Apertis device that is not
already required for other scenarios.

A local attacker (the user) could alter the real-time clock to try to
extend a time-limited bundle's validity. For example, suppose an access
token is valid from 2016-01-01 00:00:00+0000 to 2016-01-31
00:00:00+0000; on 2016-02-01, the attacker could reset the clock to
2016-01-01 00:00:00+0000 to get another 30 days of validity for an
application that has already expired. Similarly, on 2016-01-16, the
attacker could reset the clock to 2016-01-01 to get another 15 days of
validity for an application that has not expired yet.

The application manager should not allow launching an application unless
its access tokens are consistent with the time that is current at the
time of launching. However, we are not assuming that the real-time clock
is trustworthy, and time-limited access with an untrusted clock is a
difficult problem. The worst-case scenario under our
[Assumptions](#assumptions) is as follows:

  - The real-time clock is entirely attacker-controlled
  - The real-time clock can be altered while the CE domain is switched
    off, either via the automotive domain or by accessing the hardware
  - The attacker resets the real-time clock to the same value before
    every boot
  - There is no trusted time source such as GPS, cellular networks or
    anything Internet-connected, and we receive app-bundles via USB
    storage

In this worst-case scenario, the best we can do is to count how long the
CE domain has been running since the app-bundle was received, and make
it expire after 30 days of uptime. With a trusted time source, we can do
somewhat better than this.

We suggest re-examining the assumptions to determine whether any
stronger assumptions can be made (for example, a trustworthy real-time
clock). If they cannot, we suggest the rather elaborate approach
outlined in the next section.

### Partially preserving time-limited access in the face of an untrusted time-source

  - Maintain an *uptime counter* as follows:
      - During boot, load the *stored uptime counter* from storage. If
        none has been stored yet, its value is 0.
      - The uptime counter is the stored uptime that was loaded during
        boot, plus the monotonic timestamp (seconds since boot) that is
        provided by Linux. It represents the total time the device has
        been running.
  - During clean shutdown, and periodically during runtime (so that it
    can be used after an unclean shutdown), save the uptime counter as
    the stored uptime counter. This ensures that the uptime counter
    never decreases, and is always less than or equal to the device's
    total runtime. (For example, if the device is shut down uncleanly 1
    minute after updating the stored uptime counter, then the uptime
    counter will be 1 minute behind what it should be from then on.)
  - Also maintain a lower bound for the real-time clock, as follows:
      - We will sometimes receive a *trusted lower bound* for the real
        time, such as a signed access token from the app-store that is
        declared to have been generated at a particular time. Keep a
        record of the newest *trusted lower bound* that we have ever
        received, and the *uptime counter* at the time we received it.
        "Newest" here means the most recent time declared by the
        app-store, not the time from the access token that we processed
        most recently; this is to avoid our time estimate going
        backwards if a user downloads multiple app-bundles and installs
        them in a different order.
      - The *lower bound* is the newest trusted lower bound, plus the
        uptime counter now, minus the uptime counter when we received
        the newest trusted lower bound.
      - For example, suppose the user downloads app A to USB storage on
        2016-01-11, downloads app B to USB storage on 2016-01-12,
        installs app B when our uptime counter was 100 days, and
        installs app A when our uptime counter was 103 days. Suppose our
        uptime counter is now 105 days. The *trusted lower bound* is
        2016-02-12, received 5 days of uptime ago, so the resulting
        *lower bound* is 2016-01-17. If app C was meant to expire on
        2016-01-15, we now know that it has indeed expired.

We suggest that the application manager should treat applications as
expired unless their expiry dates are newer than both the real-time
clock and the *lower bound*. Effectively, the lower bound puts a limit
on how much the attacker can stretch the lifetime of an app-bundle by
resetting the clock.

The *trusted lower bound* can also be used as input into TLS certificate
validity decisions, for example in HTTPS: if a TLS certificate's expiry
date is prior to the trusted lower bound, then we know that it has
expired, even if the real-time clock says otherwise.

When deciding whether particular time-sources can be used as a *trusted
lower bound*, we must bear in mind that an attacker able to influence
these time-sources can use them as a long-term denial of service attack.
For example, if an attacker can spoof GPS or GSM signals and tell the
Apertis device that the date is sometime in 2020, and the Apertis device
treats this as a trusted lower-bound, then that device will not be able
to use any app-bundle that is meant to expire earlier than 2020. Once
carried out, this attack cannot be reversed without either real-time
Internet access, or building in a vulnerability to replay attacks. As
such, we recommend that only strongly-authenticated time-sources are
used for the *trusted lower bound*.

If the real-time clock becomes inconsistent with the *lower bound*, the
requirement that accidental incorrect setting of the real-time clock can
be "forgiven" by resetting the clock means that we cannot force the
app-bundles to expire. There is a trade-off between usability by
non-malicious users who might set the clock incorrectly, and the
inability of malicious users to bypass time limits. One possibility to
avoid this issue would be to ensure that the settings HMI makes it
impossible to set the real-time clock earlier than the lower bound.

Depending on the approach chosen for that trade-off, another possibility
would be to flag implausible real-time clock values (earlier than the
lower bound) as a possible attack, disable all app-bundles that have a
limited expiry time, and require the user to re-activate those
app-bundles by downloading new access tokens; if desired, the app-store
could flag store accounts that have done this for closer examination.
However, if this happens to a user who is not attempting to attack the
system but has merely set the clock incorrectly, this is likely to lead
to user resentment and bad PR. We recommend considering this trade-off
carefully.

## Implementation of developer access

Some of the states described in [Developer
access](#developer-access) result in Before the device is
put in a state where it can no longer be trusted to enforce conditional
access, all application bundles that are subject to conditional access
must be deleted. We recommend that this is presented as a whole-device
"factory reset" (data wipe), similar to the reset that is implied by
[unlocking the bootloader of Google's Nexus reference devices for
Android](http://www.androidcentral.com/how-unlock-galaxy-nexus-bootloader).
This satisfies the requirement that app-bundle contents not be exposed.

After resetting, the device key will have been deleted. This satisfies
the requirement that app-bundles with conditional access are not
installable: the device will be unable to decrypt the files that
encapsulate those app-bundles, because it no longer has its provisioned
device key.

We recommend that SDK/development images, and most target images for the
open-source Apertis platform, are shipped in an "unlocked" state where
conditional access is unavailable but a reset/wipe is not needed. We
recommend additionally producing one target image for the open-source
Apertis platform that is provisioned with a well-known device key, or
instructions for doing so, so that conditional access can be tested
against that device key.

If a developer device needs to be put back into a "production" mode, for
example because it is returned to a trusted manufacturer or dealer for
repair, the trusted party could wipe its contents, install a trusted
version of the OS, and re-provision it with a newly-generated device key
that is also sent to the app-store. This would return the device to a
state where it can be trusted to enforce conditional access.

If the device key is situated in [sealed
storage](https://en.wikipedia.org/wiki/Trusted_Computing#Sealed_storage),
for example provided by a TPM (Trusted Platform Module), the system
would not necessarily be required to delete it during the switch to
developer mode; it could merely make the device key inaccessible until
the OS is replaced by a trusted one. However, it would still be
necessary to delete app-bundles during the switch to developer mode.

This is somewhat linked to
[Verified boot]( {{< ref "system-updates-and-rollback.md#verified-boot" >}} )
("secure boot"), which can verify whether the operating system being
booted is a trusted one or not.

## Implementation of themes

For themes with time-limited access, the application manager should
unmap the theme from the themes directory (delete the symbolic link or
unmount the bind-mount, as applicable) when the use time expires. It
should also change the theme setting, if the expired theme was the
current one; this theme change should be propagated to applications in
the same way as any other theme change. For simplicity, we recommend
resetting the theme to the factory default when this is done.

If a reset to the most recently used non-expired theme is required, we
recommend that the application manager and the configuration application
cooperate to maintain a list of recently-used themes, with some
arbitrary cutoff (perhaps a maximum of 10 themes in the history). In the
unlikely situation that all of the themes in the history have expired or
been removed, the factory default should be assumed.

The same list of recently-used themes could be used when a current theme
is uninstalled, if desired.

The configuration application should not allow empty/deleted themes to
be selected.
