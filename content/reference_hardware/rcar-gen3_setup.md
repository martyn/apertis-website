+++
date = "2019-03-14"
lastmod = "2020-12-17"
weight = 100
toc = true

title = "R-Car Hardware Setup"

aliases = [
    "/reference_hardware/rcar-gen3_test_automation.md",
    "/old-wiki/Reference_Hardware/Rcar-gen3_test_automation",
    "/old-wiki/Reference_Hardware/Rcar-gen3_setup"
]
+++

# Requirements

You should have at least:

  - 1 supported R-car board
  - 1 suitable Power adaptor
  - 1 USB to micro-usb cable
  - ATF /optee setup on the board and booting from the A57 CPU (See
    the [documentation](https://elinux.org/R-Car/Boards/H3SK#Flashing_firmware)
    on how to set this up)

You need to provide your own:

  - 1 microSD card (at least 8GB)

## Prepare SD card

  - Download the
    [arm64 minimal image](https://images.apertis.org/release/v2020/v2020.3/arm64/minimal/apertis_v2020-minimal-arm64-uboot_v2020.3.img.gz),
    plus the [`.bmap` file](https://images.apertis.org/release/v2020/v2020.3/arm64/minimal/apertis_v2020-minimal-arm64-uboot_v2020.3.img.bmap).
    microSD card using command:

        sudo bmaptool copy apertis_v2020-minimal-arm64-uboot_v2020.3.img.gz /dev/mmcblk0

    Alternatively bmaptool can flash directly from http e.g. by using:

        sudo bmaptool copy https://images.apertis.org/release/v2020/v2020.3/arm64/minimal/apertis_v2020-minimal-arm64-uboot_v2020.3.img.gz /dev/mmcblk0


  - Put the SD card in the board

## Setup instructions

The 64 bit Apertis images use the
[Generic Distro Configuration Concept](https://gitlab.denx.de/u-boot/u-boot/-/blob/master/doc/README.distro)
from u-boot. Unfortunately this is not currently supported by the standard
Renesas u-boot, as such at least u-boot needs to be updated.

### Updating U-Boot

R-Car boards are shipped with different IPL/DRAM setup versions (part of the
[arm-trusted-firmare](https://github.com/renesas-rcar/arm-trusted-firmware)
Renesas repository) depending on the
time of productions. Apertis doesn't currently supply builds for those
components (hence the requirement to already have that setup). The following
instructions assume an IPL and Secure monitor revision newer than 1.0.10

To flash u-boot, first determine if the current revision is new enough; The
output show below is revision 1.0.12 on a StarterKit Pro:

    NOTICE:  BL2: R-Car Gen3 Initial Program Loader(CA57) Rev.1.0.12
    NOTICE:  BL2: PRR is R-Car M3 ES1.0
    NOTICE:  BL2: Boot device is HyperFlash(80MHz)
    NOTICE:  BL2: LCM state is CM
    NOTICE:  BL2: AVS setting succeeded. DVFS_SetVID=0x52
    NOTICE:  BL2: DDR3200(rev.0.20)[COLD_BOOT]..0
    NOTICE:  BL2: DRAM Split is OFF
    NOTICE:  BL2: QoS is default setting(rev.0.16)
    NOTICE:  BL2: v1.3(release):a80bbf3
    NOTICE:  BL2: Built : 22:16:12, Mar 30 2017
    NOTICE:  BL2: Normal boot
    NOTICE:  BL2: dst=0xe6316190 src=0x8180000 len=512(0x200)
    NOTICE:  BL2: dst=0x43f00000 src=0x8180400 len=6144(0x1800)
    NOTICE:  BL2: dst=0x44000000 src=0x81c0000 len=65536(0x10000)
    NOTICE:  BL2: dst=0x44100000 src=0x8200000 len=524288(0x80000)
    NOTICE:  BL2: dst=0x50000000 src=0x8640000 len=1048576(0x100000)

U-boot binaries to flash can be found on the
[images site](https://images.apertis.org/release/v2021dev1/v2021dev1.0/firmware/u-boot/).
The following table shows the file to get for the various types of boards:

| Hardware                       | u-boot path                                                                                                                                       |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| R-car Starter Kit Pro (M3)     | [r8a7796_ulcb/u-boot-elf.srec](https://images.apertis.org/release/v2021dev3/v2021dev3.0/firmware/u-boot/r8a7796_ulcb/u-boot-elf.srec)             |
| R-car Starter Kit Premier (H3) | [r8a7795_ulcb/u-boot-elf.srec](https://images.apertis.org/release/v2021dev3/v2021dev3.0/firmware/u-boot/r8a7795_ulcb/u-boot-elf.srec)             |
| R-car Salvator-X H3            | [r8a7795_salvator-x/u-boot-elf.srec](https://images.apertis.org/release/v2021dev3/v2021dev3.0/firmware/u-boot/r8a7795_salvator-x/u-boot-elf.srec) |
| R-car Salvator-X M3            | [r8a7796_salvator-x/u-boot-elf.srec](https://images.apertis.org/release/v2021dev3/v2021dev3.0/firmware/u-boot/r8a7796_salvator-x/u-boot-elf.srec) |

To flash first configure the dip switches on the board to boot into the
monitor. The exact configuration depends on the board and can be found in the
setup manual. For reference for the board available to us this is:

| Hardware            | Jumper Settings                                                                            |
| --------------------| ------------------------------------------------------------------------------------------ |
| Starter Kit Pro     | SW1=OFF, SW6\[1\]=OFF, SW6\[2\]=OFF, SW6\[3\]=OFF, SW6\[4\]=ON, JP1 -\> 1-2 short          |
| Starter Kit Premier | SW1=OFF, SW6\[1\]=ON, SW6\[2\]=ON, SW6\[3\]=OFF, SW6\[4\]=ON                               |
| Salvator-X H3       | SW1=ON, SW2=ON, SW3=OFF, SW13=PIN1, SW10\[5\]=ON, SW10\[6\]=OFF,SW10\[7\]=ON, SW10\[8\]=ON |

After changing these settings, connect to the serial console (using e.g.
minicom), power on the board (note: some boards have a power button that must be pressed) and the mini loader should come up.

1.  execute `xls2` on the board to put into receiving mode
2.  Choose `3` (hyperflash)
3.  Configure the switches as suggested by the monitor and input `y`
    1.  On the Starter Kit Premier, [the reference page](https://elinux.org/R-Car/Boards/H3SK#Flashing_firmware)
        mentions to change SW1=ON, SW6\[1\]=OFF, SW6\[3\]=ON.
4.  Input the memory destination address: `50000000`
5.  Input the flash address: `640000`
6.  Upload the u-boot.srec using ascii style tranfer (ctrl+A S in
    minicom)
7.  After upload indicate ok to clear: `y`

Afterwards ensure that the board is configured to boot from hyperflash
and reset the board. To do this on the boards currently availble to us:

| Hardware            | Jumper Settings                                          |
| ------------------- | -------------------------------------------------------- |
| Starter Kit Pro     | SW6[ALL]=ON                                              |
| Starter Kit Premier | SW6[ALL]=ON (this enables DDR3200)                       |
| Salvator-X H3       | SW10\[5\]=ON, SW10\[6\]=ON, SW10\[7\]=OFF, SW10\[8\]=OFF |

It is recommended to reset the U-Boot environment after flashing new version of bootloader.
Stop in U-Boot prompt and execute commands:

    setenv default -a
    saveenv

After reset the board should now boot Apertis from SD card automatically.

### Flashing image to eMMC

SD cards can be slow to use. Given all Renesas board have a nice eMMC on
board it makes sense to use that instead the simplest way to do that is
to use an apertis image installed on SD card.

1.  Enable development mode by running **apertis-dev**
2.  Install the bmap-tools package **apt install bmap-tools**
3.  If not flashing directly from http download the intended Image to SD
    card
4.  Flash image to emmc (typically /dev/mmcblk1): **bmaptool copy
    <image location> /dev/mmcblk1**
5.  Remove the SD card (u-boot will prefer booting removable media), and
    restart the board

# Setting up a R-car device for LAVA

For test automation it is required that a boards can be automatically
powered on and off, for most boards this is done by simply cutting the
power supply. However for Renesas boards it is strongly recommended not
to do this. As such a different solution is needed.

Luckily for both Salvator-X and Starter Kit boards there are connectors
exposed which allow remote on/off. Both accept 3.3v which means e.g.
gpio's on a raspberry pi can be used for control.

## R-car Salvator X

Connector: SAMTEC QTE-040-03-L-D-A available from Farnell:
<http://nl.farnell.com/samtec/qte-040-03-l-d-a/header-dual-80way/dp/1667888>

The Salvator-X board as an EX_PWRon line on the EXIO Connector D on the
bottom of the board available, which internally connects to a FET which
can force the board to be off if the board if the EX_PWRon line is
forced to high.

For control add the GPIO line to pin 68 of the connector and a ground
connection to pin 58 of the connector, once hooking up the ground and a
gpio in the raspberry pi the board can be forced off/in reset (similar
to the action of the physical switch) by turing the GPIO high and to
come back on by setting the gpio to low.

![<File:Salvator-x.jpg>](/images/salvator-x.jpg)
![<File:Salvator-x-connector.jpg>](/images/salvator-x-connector.jpg)

## R-car Starter Kit

Connector: EPT 402-51501-51 Stacking Board Connector available from
Farnell:
<http://nl.farnell.com/ept/402-51501-51/conn-stacking-rcpt-440pos-0-5mm/dp/2474236>

The Starter kit is even more tricky, by default it will not turn on on
power on only after pressing the reset button. To automate control, as
explained in section 3.18 of the Hardware Manual. The reset mode pin
(A18 on the CoM Express connector) should be clamped low (e.g. connector
to ground, pin A21 is conveniently close). Once that's done Pin A15
(EX_PWRon) can be used to control the boards power state (high is on,
low is off).

![<File:StarterKit.jpg>](/images/starterkit.jpg)
![<File:Starterkit-connector.jpg>](/images/starterkit-connector.jpg)

# References

  - [General Renesas R-car family information](http://elinux.org/R-Car)

# Known issues

  - In case if the boot has stopped after loading the kernel, initramfs
    and dtb with string `Starting kernel ...`

    Check the values of `kernel_addr_r` and `ramdisk_addr_r` -- there
    should be enough space to load the kernel.

    Probably you need to increase `ramdisk_addr_r` value to avoid
    overlapping of the kernel in memory. For example:

        kernel_addr_r=0x48080000
        ramdisk_addr_r=0x4A000000
