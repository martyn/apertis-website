# Apertis Website

This is the source for the main Apertis website. It is generated using
[Hugo](https://gohugo.io/) with a modified
[Beautiful Hugo theme](https://github.com/halogenica/beautifulhugo), changing
the look, implementing search and allowing for the generation of PDFs. The page
is served from GitLab pages.

---

## Document layout

When adding documents to the Apertis Website, please adhere to the following
layout guidelines:

- Policies:
  - Documenting procedures and rules
  - Minimal requirements for project involvement
- Concepts:
  - Topics that have been researched and/or planned but which haven't yet been
    implemented
- Architecture:
  - Description of project infrastructure
  - Details of technologies and software used by Apertis
- Guides:
  - Guidance on using specific software features
  - Worked examples of expected project workflows
- QA:
  - Test reports
  - Test procedures (realistically, a description of the testing performed and
    a pointer to qa.apertis.org)
- Releases:
  - Release notes
  - Release schedules

## Spelling

In order to provide some consistency and quality to the website, we would like
to ensure that all documents have been spellchecked. In the mid-term we would
like checks to be performed as part of the websites CI/CD, using Aspell to
check the spellings. As it is likely that many of the documents use words not
in Aspell's dictionaries, we are starting with a manual approach to start
building up a personal dictionary of additional words that the Apertis project
is happy with.

When making changes or adding documents, please run:

    aspell --personal="./dictionary.aspell" --lang="en_us" --mode=markdown check <document>

Any issues caught should be rectified or added to the `dictionary.aspell` file
(maintaining the alphabetical order being used).

## Use of Hugo `ref` Shortcode

Hugo provides the `ref` shortcode to aid with
[creating links between documents](https://gohugo.io/content-management/cross-references/).
This should be used for all references internal to the website. The generation
process will fail when the target page of the reference can not be found,
causing the CI to fail and thus aids with reducing the risk broken links being
provided on the website.

## Adding responsive tables

The Apertis theme provides a shortcode to wrapper tables to be more resposnive when resizing, splitting the table down into blocks on small screen sizes. This can be enabled by wrapping the table in the `responsive-table` shortcode:

```
{{% responsive-table %}}

| Item  | Status       |
|-------|--------------|
| `foo` | Badgers      |
| `bar` | More badgers |

{{% /responsive-table %}}
```

## Formatting

Please follow these guidelines to enable us to maintain some consistency across the documentation:

1. **Don't add an "Introduction" heading as the first line in the document**:
   It is generally understood that the first block of text in a document will
   introduce what follows, the title is redundant.

2. **Titles should be in
   [Title Case](https://en.wikipedia.org/wiki/Title_case)**: This is a widely
   used convention.

3. **Use the
   [primary Markdown format for titles](https://www.markdownguide.org/basic-syntax/#headings)**:
   The expected title format is appending one or more `=` to the beginning of
   the line holding the title.  This format has greater flexibility than the
   alternative form.

4. **Use
   [indented code blocks](https://www.markdownguide.org/basic-syntax/#code-blocks)
   for command line examples**: These should be indented with 4 spaces (beyond
   any existing indentation, such as for lists).

5. **Use
   [Fenced Code Blocks](https://www.markdownguide.org/extended-syntax/#fenced-code-blocks)
   for file contents**: These should be marked with a syntax highlighting hint
   where appropriate and typically the `numberLines` hint to cause the blocks
   to be rendered with line numbers. For example, for C source code:

       ``` c numberLines
       #include <stdio.h>

       int main(int argc, char argv*)
       {
           print("Hello World\n");
           return 0;
       }
       ```

6. **Try and keep to 80 character width lines**: This makes it easier for some
   to read. It is questionable whether this helps with viewing changes, as
   rewrapping can make changes unclear, but gitlab currently doesn't do a
   great job of rendering changes to long lines, so this is the style that's
   been chosen.

   **Note**: Please keep Markdown URLs on a single line, even if this stretches
   the line beyond 80 characters as it helps with parsing/scripting URL checks
   etc.

## Generating PDFs

In order to generate PDFs, we are getting Hugo to create a simplified HTML
pages. So as not to have every page generating a PDF, to get a page to be
generated as a PDF (and HTML at the same time) add the following to the front
matter of the page:

```
outputs = ["html", "pdf-in"]
```

This will result in the simplified HTML being produced in a file called
`index.pdf-in` in the relevant directory. The CI is configured to look for
these files once Hugo has generated the site and create PDFs of them. For the
page `www.apertis.org/concepts/foo/`, a PDF will be available as
`www.apertis.org/concepts/foo/foo.pdf`.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

The `run_hugo.sh` script is provided to run Hugo from docker. Running this
script will generate the site, running `./run_hugo.sh server` will serve the
site locally on `http://localhost:1313`.

### Preview your site

If you clone or download this project to your local computer and run `hugo
server`, your site can be accessed under `localhost:1313/hugo/`.

